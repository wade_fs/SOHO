package com.acim.acim_cn;

import java.io.Serializable;

public class Comment implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -952065886803166450L;
	//mode 0 = text, mode 1 = with recording
	public int mode;
	public String comment;
	public String time;
	
	Comment(){
		mode = 0;
		comment = "";
	}
}
