package com.acim.acim_cn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Receiver extends BroadcastReceiver {

 
	@Override
	public void onReceive(Context context, Intent intent) {

		Intent service = new Intent(context, AlarmService.class);
		context.startService(service);
	}
 
 
}
