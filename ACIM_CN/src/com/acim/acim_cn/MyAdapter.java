package com.acim.acim_cn;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter{

	LayoutInflater lay;
	
	public MyAdapter() {
		// TODO 自動產生的建構子 Stub
		lay = null;
	}
	
	public MyAdapter(LayoutInflater l2){
		this.lay = l2;
	}
	
	@Override
	public int getCount() {
		// TODO 自動產生的方法 Stub
		return MainActivity.lessonlist.lesson_list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO 自動產生的方法 Stub
		return MainActivity.lessonlist.lesson_list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO 自動產生的方法 Stub
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO 自動產生的方法 Stub
		View row;
		row = lay.inflate(R.layout.outline_list, parent, false);
		ImageView iv = (ImageView) row.findViewById(R.id.outline_book);
		TextView head = (TextView) row.findViewById(R.id.outline_head);
		TextView sub = (TextView) row.findViewById(R.id.outline_subject);
		TextView posi = (TextView) row.findViewById(R.id.outline_posi);
		posi.setVisibility(View.GONE);
		posi.setText(String.valueOf(position));
		
		if(MainActivity.lessonlist.lesson_list.get(position).getBook()){
			iv.setVisibility(View.VISIBLE);
		} else {
			iv.setVisibility(View.INVISIBLE);
		}
		row.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO 自動產生的方法 Stub
				
				ImageView b = (ImageView)v.findViewById(R.id.outline_book);
				TextView poi = (TextView)v.findViewById(R.id.outline_posi);
				Log.e("acim", poi.getText().toString());
				int posi = Integer.valueOf(poi.getText().toString()).intValue();
				float v_x = b.getX();
				Log.e("acim" , String.valueOf(v_x));
				float v_y = b.getY();
				Log.e("acim" , String.valueOf(v_y));
				float v_sx = b.getWidth();
				Log.e("acim" , String.valueOf(v_sx));
				float v_sy = b.getHeight();
				Log.e("acim" , String.valueOf(v_sy));
				Log.e("acim" , event.toString());
				if(event.getX() > v_x && event.getX() < v_x+v_sx){
					if(event.getY() > v_y && event.getY() < v_y+v_sy){
						if(event.getAction() == MotionEvent.ACTION_UP){
							Log.e("acim", "change");
							if(MainActivity.lessonlist.lesson_list.get(posi).getBook()){
								MainActivity.lessonlist.lesson_list.get(posi).setBook(false);								
							} else {
								MainActivity.lessonlist.lesson_list.get(posi).setBook(true);
							}
							if(MainActivity.lessonlist.current_lesson.getPos() == posi){
								if(MainActivity.lessonlist.current_lesson.getBook()){
									MainActivity.lessonlist.current_lesson.setBook(false);								
								} else {
									MainActivity.lessonlist.current_lesson.setBook(true);
								}
							}
							MainActivity.Set_bookmark_visible();
							new Handler().post(new Runnable() {
								
								@Override
								public void run() {
									// TODO 自動產生的方法 Stub
									MainActivity.myada.notifyDataSetChanged();
								}
							});
						}
						return true;
					}
				}
				
				return false;
			}
		});
		
		head.setText(MainActivity.lessonlist.lesson_list.get(position).getHead());
		head.setSingleLine(true);
		sub.setText(MainActivity.lessonlist.lesson_list.get(position).getSub());
		sub.setSingleLine(true);
				
		return row;
	}

}
