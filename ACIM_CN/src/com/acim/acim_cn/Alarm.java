package com.acim.acim_cn;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class Alarm {

    private Context context;
    private AlarmManager am;
    
    Alarm(Context c, AlarmManager a){
    	this.context = c;
    	this.am = a;
    }
    
    public void setAlarm(long starttime_ms, int times, long period_ms, boolean vibrate){
    	
    	SharedPreferences pref = context.getSharedPreferences("alarm", 0);
    	SharedPreferences.Editor prefEdt = pref.edit(); 
    	prefEdt.putBoolean("vibrate", vibrate);
    	prefEdt.putInt("lefttime", times);
    	prefEdt.putBoolean("switch", true);
    	Intent intent = new Intent(context.getApplicationContext(), Receiver.class);
    	PendingIntent p = PendingIntent.getBroadcast(context.getApplicationContext(), 9817050, intent, 0);
    	am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+starttime_ms, period_ms, p);
    	prefEdt.commit();
    }

    public void changeMode(boolean vibrate){
    	
    	SharedPreferences pref = context.getSharedPreferences("alarm", 0);
    	SharedPreferences.Editor prefEdt = pref.edit(); 
    	prefEdt.putBoolean("vibrate", vibrate);
    	prefEdt.commit();   
    }
    
    public void setSwitch(boolean Switch){
    	SharedPreferences pref = context.getSharedPreferences("alarm", 0);
    	SharedPreferences.Editor prefEdt = pref.edit(); 
    	prefEdt.putBoolean("switch", Switch);
    	prefEdt.commit();
    }

    public void cancelAll(){
    	
    	Intent intent = new Intent(context.getApplicationContext(), Receiver.class);
    	PendingIntent p = PendingIntent.getBroadcast(context.getApplicationContext(), 9817050, intent, 0);
    	am.cancel(p);
    	SharedPreferences pref = context.getSharedPreferences("alarm", 0);
    	SharedPreferences.Editor prefEdt = pref.edit(); 
    	prefEdt.putBoolean("switch", false);
    	prefEdt.commit();
    }
    
}
