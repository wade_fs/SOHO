package com.acim.acim_cn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class Lesson_List implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 123;

	public List<Lesson> lesson_list = new ArrayList<Lesson>();
	public Lesson current_lesson = new Lesson();

	private int lesson = 390;
	public int text_size_ratio = 10;
	public boolean alarm_switch = false;
	public boolean vibrate = false;
	public boolean custom_alarm = false;
	public int alarm_lesson = 0;
	
	public Lesson_List(){
		
	}
	
	public void load_lesson(){
		for(int i = 0; i<=lesson; i++){
			get_lesson(i, true);
			lesson_list.get(i).setPos(i);
			Log.e("acim", String.valueOf(lesson_list.get(i).getPos()));
		}
		lesson_list.add(new Lesson());
	}
	
	public void load_context(){
		Lesson l = new Load_lesson(current_lesson.filename, false).getLesson();
		current_lesson.context = l.getCon();
	}
	
	private void get_lesson(int l, boolean n){
		String s = "wb";
		s = s + String.valueOf(l/100);
		l = l%100;
		s = s + String.valueOf(l/10);
		l = l % 10;
		s = s + String.valueOf(l);
		Log.e("Lesson_List: ", s);
		if(n){
			lesson_list.add(new Load_lesson(s, n).getLesson());
		} else {
			
		}
	}
}
