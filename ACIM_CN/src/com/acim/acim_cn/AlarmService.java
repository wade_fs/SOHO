package com.acim.acim_cn;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

public class AlarmService extends Service {

	private NotificationManager mManager;
	private MediaPlayer mp;
	private Vibrator vVi;
	AlertDialog ad;
    @Override
    public IBinder onBind(Intent arg0){
       // TODO Auto-generated method stub

        return null;
    }
 
    @Override
    public void onCreate(){
       // TODO Auto-generated method stub  
       super.onCreate();       
    }
 
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleStart(intent, startId);
        return START_NOT_STICKY;
    }

    @SuppressWarnings({ "static-access", "deprecation" })
	void handleStart(Intent intent, int startId) {
    	 
    	SharedPreferences pref = getSharedPreferences("alarm", 0);
    	SharedPreferences.Editor prefEdt = pref.edit();
    	
    	int lefttime = pref.getInt("lefttime", 0);
    	lefttime = lefttime - 1;
    	
    	if(pref.getBoolean("switch", false)==true){
    		Notification notification = new Notification(R.drawable.c48icon,"奇蹟課程", System.currentTimeMillis());
    		
    		if(pref.getBoolean("vibrate", false)==true){
    			vVi = (Vibrator)this.getSystemService(Service.VIBRATOR_SERVICE);
    			vVi.vibrate( new long[]{0, 1000, 500}, -1);
    		}	
    		else {
    			//mp = MediaPlayer.create(AlarmService.this, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
    			//mp = MediaPlayer.create(AlarmService.this, Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.acim_clock));
    			//mp.setLooping(true);
    			//mp.start();
    			notification.sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.acim_clock);
    			notification.flags |= notification.FLAG_AUTO_CANCEL;
    		}        	
        	mManager = (NotificationManager) this.getApplicationContext().getSystemService(this.getApplicationContext().NOTIFICATION_SERVICE);
            Intent pintent = new Intent(this.getApplicationContext(),MainActivity.class);
                        
            pintent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_CLEAR_TOP);
      
            PendingIntent pendingNotificationIntent = PendingIntent.getActivity( this.getApplicationContext(),0, pintent,PendingIntent.FLAG_UPDATE_CURRENT);
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.setLatestEventInfo(this.getApplicationContext(), "奇蹟課程", "奇蹟課程提醒：您該練習了。(剩餘" + String.valueOf(lefttime) + "次)", pendingNotificationIntent);                         
            mManager.notify(0, notification);                       
    	}
    	
    	prefEdt.putInt("lefttime", lefttime);
    	if(lefttime<1){
    		prefEdt.putBoolean("switch", false);
    		Intent i = new Intent(getApplicationContext(), Receiver.class);
    		PendingIntent p = PendingIntent.getBroadcast(getApplicationContext(), 9817050, i, 0);
    		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
    		am.cancel(p);
    	}
    	prefEdt.commit();
    	
    	AlertDialog.Builder adb = new AlertDialog.Builder(this);
    	adb.setTitle("奇蹟課程提醒");
    	adb.setMessage("奇蹟課程提醒：您該練習了");
    	adb.setPositiveButton("確認", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO 自動產生的方法 Stub
				mManager.cancel(0);
			}
		});
    	ad = adb.create();
    	ad.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);  
    	ad.setCanceledOnTouchOutside(false);
    	ad.show();
    	
    	Handler h = new Handler();
    	h.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Log.e("acim","check ad showing");
				if(ad.isShowing()){
					Log.e("acim","close ad");
					ad.dismiss();
				}
			}
		}, 10000);
    }  
    
   @Override
   public boolean onUnbind(Intent intent) {
	   // TODO Auto-generated method stub
	   return super.onUnbind(intent);
   }   
 
    @Override
    public void onDestroy(){
        // TODO Auto-generated method stub
    	stopSelf();
        super.onDestroy();
    }
	
}
