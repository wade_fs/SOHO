package com.acim.acim_cn;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;

public class Load_lesson {

	Lesson l;
	boolean ne;
	XmlResourceParser xrp;
	
	Load_lesson(String xmlName, boolean n){
		Resources r = MainActivity.res;
		xrp = r.getXml(r.getIdentifier(xmlName, "xml", MainActivity.packetName));
		//xrp = r.getXml(R.xml.lesson1);
		ne = n;
		try {
			xrp.next();
			l = new Lesson();
			l.filename = xmlName;
			//Log.e("loadXml", "1");
			while(xrp.getEventType() != XmlPullParser.END_DOCUMENT){
				//Log.e("loadXml", "2");
				if(xrp.getEventType() == XmlPullParser.START_TAG && xrp.getName().equals("program")){
					//Log.e("loadXml", "<program>");
					main();
					
				}
				xrp.next();
			}
		} catch (XmlPullParserException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		}		
	}
	
	private void main() throws XmlPullParserException, IOException{
		xrp.next();
		while(!(xrp.getEventType() == XmlPullParser.END_TAG && xrp.getName().equals("program"))){
			if(xrp.getEventType() == XmlPullParser.START_TAG){
				if(xrp.getName().equals("head")){
					//Log.e("loadXml", "<head>");
					getHead();
				} else if (xrp.getName().equals("subject")){
					//Log.e("loadXml", "<subject>");
					getSubject();
				} else if (xrp.getName().equals("context")){
					//Log.e("loadXml", "<context>");
					getContext();
				} else if (xrp.getName().equals("alarm")){
					//Log.e("loadXml", "<alarm>");
					getAlarm();
				} else {
					//Log.e("acim", "erro");
				}
			}
			xrp.next();
		}
		//Log.e("loadXml", "</program>");
	}
	
	private void getHead() throws XmlPullParserException, IOException{
		xrp.next();
		if(xrp.getEventType() == XmlPullParser.TEXT){
			l.head = xrp.getText();
			//Log.e("loadXml", l.head);
			xrp.next();
		}		
		//Log.e("loadXml", "</head>");
	}
	
	private void getSubject() throws XmlPullParserException, IOException{
		xrp.next();
		if(xrp.getEventType() == XmlPullParser.TEXT){
			l.subject = xrp.getText();
			//Log.e("loadXml", l.subject);
			xrp.next();
		}		
		//Log.e("loadXml", "</subject>");
	}
	
	private void getAlarm() throws XmlPullParserException, IOException{
		xrp.next();
		if(xrp.getEventType() == XmlPullParser.TEXT){
			String s = xrp.getText();
			String[] s2 = s.split(",");
			l.alarm[0] = s2[0];
			l.alarm[1] = s2[1];
			l.alarm[2] = s2[2];
			//Log.e("loadXml", s);
			xrp.next();
		}		
		//Log.e("loadXml", "</alarm>");
	}
	
	private void getContext() throws XmlPullParserException, IOException{
		xrp.next();
		Order_list ol = null;
		boolean pra = false;
		while(!(xrp.getEventType() == XmlPullParser.END_TAG && xrp.getName().equals("context"))) {
			if((xrp.getEventType() == XmlPullParser.START_TAG && xrp.getName().equals("order_list")) || (xrp.getEventType() == XmlPullParser.START_TAG && xrp.getName().equals("p"))) {
				//Log.e("loadXml", "<order_list>");
				//getOrderList();
				if(ol!=null) l.context.add(ol);
				ol = new Order_list();
				if(xrp.getName().equals("p")){
					xrp.next();
					if (xrp.getEventType() == XmlPullParser.START_TAG && xrp.getName().equals("sup")){
						pra = false;
						String sup = "";
						String text = "";
						xrp.next();
						if(xrp.getEventType() == XmlPullParser.TEXT) {
							sup = xrp.getText();
							xrp.next();
						}
						xrp.next();
						if(xrp.getEventType() == XmlPullParser.TEXT) {
							text = xrp.getText();
							xrp.next();
						}
						if(!ne) ol.order_list.add(new Order(sup, text, true));
					} else {
						pra = true;
					}
				}
			} else if((xrp.getEventType() == XmlPullParser.END_TAG && xrp.getName().equals("order_list")) || (xrp.getEventType() == XmlPullParser.END_TAG && xrp.getName().equals("p"))) {
				if(ol.order_list.size() == 0) {
					if(!ne) ol.order_list.add(new Order(" ", " ", false));
				}
			} else if (xrp.getEventType() == XmlPullParser.TEXT){
				if(!ne) ol.order_list.add(new Order("", xrp.getText(),false));
			}  else if(xrp.getEventType() == XmlPullParser.START_TAG && xrp.getName().equals("sup")){
				xrp.next();
				String sup = "";
				String text = "";
				if(xrp.getEventType() == XmlPullParser.TEXT){
					sup = xrp.getText();
					xrp.next();
				}
				xrp.next();
				if(xrp.getEventType() == XmlPullParser.TEXT){
					text = xrp.getText();
				}
				if(!ne) ol.order_list.add(new Order(sup, text, false));
			}
			if(!pra) xrp.next();
			else pra = false;
		}
		if(ol!=null) l.context.add(ol);
		//Log.e("loadXml", "</context>");
	}
	
	private void getOrderList() throws XmlPullParserException, IOException{
		Order_list ol = new Order_list();
		xrp.next();
		String s = "";
		String t = "";
		boolean p = false;
		while(!(xrp.getEventType() == XmlPullParser.END_TAG && xrp.getName().equals("order_list"))){
			if(xrp.getEventType() == XmlPullParser.TEXT){
				t = ToDBC(xrp.getText() + " " );
				//t.replaceAll(".", "﹒");
				//Log.e("loadXml", t);
				ol.order_list.add(new Order(s, t, p));
			} else if(xrp.getEventType() == XmlPullParser.START_TAG && xrp.getName().equals("sup")){
				//Log.e("loadXml", "<sup>");
				s = getSup();
			} else if(xrp.getEventType() == XmlPullParser.START_TAG && xrp.getName().equals("practice")){
				p = true;
			} else if(xrp.getEventType() == XmlPullParser.END_TAG && xrp.getName().equals("practice")){
				p = false;
			}
			xrp.next();
		}
		l.context.add(ol);
		//Log.e("loadXml", "</order_list>");
	}
	
	private String getSup() throws XmlPullParserException, IOException{
		String s="";
		xrp.next();
		if(xrp.getEventType() == XmlPullParser.TEXT){
			s = xrp.getText();
			//Log.e("loadXml", s);
			xrp.next();
		}
		//跳過end tag		
		//Log.e("loadXml", "</sup>");
		return s;
	}
	
	public Lesson getLesson(){
		return l;
	}
	
	public static String ToDBC(String input) {
		char[] c = input.toCharArray();
		for (int i = 0; i< c.length; i++) {
			if (c[i] == 12288) {
				c[i] = (char) 32;
				continue;
			}if (c[i]> 65280&& c[i]< 65375)
				c[i] = (char) (c[i] - 65248);
		}
		return new String(c);
	}
}
