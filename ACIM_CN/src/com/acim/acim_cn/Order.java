package com.acim.acim_cn;

import java.io.Serializable;

public class Order implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1232425039117371270L;
	public String sup;
	public String text;
	public boolean practice;
	
	Order(String s, String t, boolean p){
		this.sup = s;
		this.text = t;
		this.practice = p;
	}
}
