package view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

public class MyScrollView extends ScrollView {
	
	private boolean end;
	private boolean start;
	private boolean stop;
	private String name;

	public MyScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO 自動產生的建構子 Stub
	}

	
	public MyScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO 自動產生的建構子 Stub
	}

	
	public MyScrollView(Context context) {
		super(context);
		// TODO 自動產生的建構子 Stub
	
	}

	public boolean getStart(){
		if(getHeight() >= this.getChildAt(0).getHeight()){
			return true;
		}
		return start;
	}
	
	public boolean getEnd(){
		Log.e("acim", String.valueOf(getHeight()) + ";" + String.valueOf(this.getChildAt(0).getHeight()));
		if(getHeight() >= this.getChildAt(0).getHeight()){
			return true;
		}
		return end;
	}
	
	public void setStop(boolean t){
		this.stop = t;
	}
	
	
	public void setName(String name){
		this.name = name;
	}
	

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		// TODO 自動產生的方法 Stub
		if(stop) {
			Log.e("acim", "onScrollChanged Stop");
			return true;
		}
		return super.onTouchEvent(ev);
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		// TODO 自動產生的方法 Stub		
		//Log.e("acim", name + " onScrollChanged " + String.valueOf(l) + " " + String.valueOf(t) + " " + String.valueOf(oldl) + " "  + String.valueOf(oldt) + " ");
		View v = (View)this.getChildAt(getChildCount() - 1);
		int diff = (v.getBottom() - (this.getHeight() + this.getScrollY()));
		if(diff == 0){
			end = true;
		} else {
			end = false;
		}
		if(this.getScrollY() == 0){
			start = true;
		} else {
			start = false;
		}
		super.onScrollChanged(l, t, oldl, oldt);
	}
}
