package com.acim.acim_tw;

import view.MyScrollView;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Scroller;

public class FlyOutContainer extends FrameLayout{

	public FlyOutContainer(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public FlyOutContainer(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	private View left_column;
	private View right_column;
	private MyScrollView content;
	private MyScrollView next_content;
	private ImageView cover;
	private View right_setting;
	private ImageView right_setting_image;
	private ImageView left_outline_image;
	// part of showing when menu out
	protected static int Margin = 115;
	protected static int Buf_Margin =  35;
	protected static int Page_Margin =  33;

	//動畫的狀態
	public enum AniState { CLOSED , OPEN , CLOSING, OPENING};
	//one is content, two is next
	public enum ContentState { C, NEXTC, TOC, TONEXT, COVER};
	
	public enum State{ MAIN, L_OUTLINE, R_DETAIL, R_SETTING};
	//position of content
	protected int currentContentOffset = 0; 
	protected AniState rightColumn_CurrentState = AniState.CLOSED;
	protected AniState leftColumn_CurrentState = AniState.CLOSED;
	protected AniState rightSetting_CurrentState = AniState.CLOSED;
	protected ContentState currentContentView = ContentState.COVER;
	protected State state = State.MAIN;

	//animation
	protected Scroller AnimationScroller = new Scroller(this.getContext(), new SmoothInterpolator());
	//new SmoothInterpolator());
	private Runnable AnimationRunnable = new AnimationRunnable();
	protected Handler AnimationHandler = new Handler();

	//animation constants
	private static final int LR_AnimationDuration = 750;
	private static final int content_AnimationDuration = 2000;
	private static final int AnimationPollingInterval = 16;
	
	private boolean start = true;
	private Bitmap bm;
	private Bitmap bm_cover;
	private Bitmap bm_context;
	private Drawable dw_context;
	
	@Override
	protected void onAttachedToWindow(){
		super.onAttachedToWindow();
  
		Log.e("acim","onAttachedToWindow");
		//get right column view
		this.right_column = this.getChildAt(0);
		this.right_column.setVisibility(View.INVISIBLE);
		//get left column view 
		this.left_column = this.getChildAt(1);
		this.left_column.setVisibility(View.INVISIBLE);
		//middle content
		this.content = (MyScrollView) this.getChildAt(2);
		this.content.setVisibility(View.INVISIBLE);
		//next content
		this.next_content = (MyScrollView) this.getChildAt(3);
		this.next_content.setVisibility(View.INVISIBLE);		
		//設定畫面
		this.right_setting = this.getChildAt(4);
		this.right_setting.setVisibility(View.INVISIBLE);
		this.right_setting_image = (ImageView) this.getChildAt(5);
		
		this.left_outline_image = (ImageView) this.getChildAt(6);
		//封面
		this.cover = (ImageView) this.getChildAt(7);
		showCover();
	}


	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom)
	{
		//Log.e("acim", "onLayout" + left +"," + top + "," + right + "," + bottom);		
		if(start){
			this.calculateChildDimensions();
			start = false; 
		}
		//Log.e("acim", String.valueOf(MainActivity.width));
		Margin = MainActivity.width * 115 / 720;
		Buf_Margin =  MainActivity.width * 35 / 720;
		Page_Margin =  MainActivity.width * 33 / 720;
		
		//Log.e("acim", String.valueOf(Margin));
		//Log.e("acim", String.valueOf(Buf_Margin));
		//Log.e("acim", String.valueOf(Page_Margin));
		
		this.left_column.layout(left, top, right - Margin, bottom);

		this.right_column.layout(left + Margin, top, right, bottom);
		
		this.right_setting.layout(left + Margin, top, right, bottom);
		
		this.right_setting_image.layout(left + Margin, top, right, bottom);
		
		this.left_outline_image.layout(left, top, right - Margin, bottom);

		switch(state){
			case MAIN:				
				switch(currentContentView){
					case COVER:
						Log.e("acim","Main cover");
						this.cover.layout(left + this.currentContentOffset - Buf_Margin, top - Buf_Margin, right + this.currentContentOffset + Buf_Margin, bottom + Buf_Margin);
						this.cover.bringToFront();
						break;
					case TONEXT:
						Log.e("acim","Main tonext");
						this.content.layout(left - Buf_Margin, top + this.currentContentOffset - Page_Margin, right + Buf_Margin, bottom + this.currentContentOffset + Page_Margin);
						this.content.bringToFront();
						this.next_content.layout(left - Buf_Margin, top + getContentHeight() + this.currentContentOffset - Page_Margin, right + Buf_Margin, bottom + getContentHeight() + this.currentContentOffset + Page_Margin);
						break;
					case NEXTC:
						Log.e("acim","Main nextc");
						this.next_content.layout(left - Buf_Margin, top - Page_Margin, right + Buf_Margin, bottom + Page_Margin);
						this.next_content.bringToFront();
						this.content.layout(left - Buf_Margin, top + getContentHeight() - Page_Margin, right + Buf_Margin, bottom + getContentHeight() + Page_Margin);
						break;
					case C:
						Log.e("acim","Main c");
						this.content.layout(left - Buf_Margin, top - Page_Margin, right + Buf_Margin, bottom + Page_Margin);
						this.content.bringToFront();
						this.next_content.layout(left - Buf_Margin, top + getContentHeight() - Page_Margin, right + Buf_Margin, bottom + getContentHeight() + Page_Margin);
						break;
					case TOC:
						Log.e("acim","Main toc");
						this.content.layout(left - Buf_Margin, top + getContentHeight() + this.currentContentOffset - Page_Margin, right + Buf_Margin, bottom + getContentHeight() + this.currentContentOffset + Page_Margin);
						this.next_content.bringToFront();
						this.next_content.layout(left - Buf_Margin, top + this.currentContentOffset - Page_Margin, right + Buf_Margin, bottom + this.currentContentOffset + Page_Margin);
						break;
				}				
				break;			
			case L_OUTLINE:
			case R_DETAIL:
			case R_SETTING:
				switch(currentContentView){
					case COVER:
						Log.e("acim","lrr cover");
						this.cover.layout(left + this.currentContentOffset - Buf_Margin, top - Buf_Margin, right + this.currentContentOffset + Buf_Margin , bottom + Buf_Margin );
						this.cover.bringToFront();
						break;
					case C:
						Log.e("acim","lrr c");
						this.content.layout(left + this.currentContentOffset - Buf_Margin, top - Page_Margin, right + this.currentContentOffset + Buf_Margin , bottom + Page_Margin);						
						this.content.bringToFront();
						break;
					case NEXTC:
						Log.e("acim","lrr nextc");
						this.next_content.layout(left + this.currentContentOffset - Buf_Margin, top - Page_Margin, right + this.currentContentOffset + Buf_Margin , bottom + Page_Margin);
						this.next_content.bringToFront();
						break;
					default:
						Log.e("acim", "error");
						break;
							
				}
				break; 
		}
	}
	
	public void showCover(){
		//content.setBackground(null);
		//next_content.setBackground(null);
		//dw_context = null;
		//bm_context.recycle();
		
		content.setVisibility(View.GONE);	
		next_content.setVisibility(View.GONE);	
		cover.setVisibility(View.VISIBLE);
		BitmapFactory.Options bo = new Options();
		bo.inSampleSize = 2;
		bm_cover = BitmapFactory.decodeResource(getResources(), R.drawable.cover, bo);
		cover.setImageBitmap(bm_cover);
		cover.bringToFront();
		currentContentView = ContentState.COVER;
	}
	
	public void disCover(){
		//bm_context = BitmapFactory.decodeResource(getResources(), R.drawable.background);
		//dw_context = new BitmapDrawable(getResources(), bm_context);
		
		currentContentView = ContentState.C;
		
		content.setVisibility(View.VISIBLE);
		//content.setb
		content.setBackgroundResource(R.drawable.background);
		
		next_content.setVisibility(View.GONE);
		//next_content.setBackground(dw_context);
		next_content.setBackgroundResource(0);
		
		cover.setVisibility(View.GONE);
		cover.setImageBitmap(null);
		bm_cover.recycle();
		content.bringToFront();	
		
		System.gc();
	}

	public boolean toggleright(){		
		if(state != State.MAIN && state !=State.R_DETAIL) return false;
		if(currentContentView != ContentState.C && currentContentView != ContentState.NEXTC && currentContentView != ContentState.COVER) return false;
		state = State.R_DETAIL;
		switch(this.rightColumn_CurrentState) {
			case CLOSED:
				this.right_column.setVisibility(View.VISIBLE);
				this.rightColumn_CurrentState = AniState.OPENING;
				switch(currentContentView){
					case C:
						this.content.setEnabled(false);
						break;
					case NEXTC:
						this.next_content.setEnabled(false);
						break;
				}
				this.AnimationScroller.startScroll(0, 0, -this.getRightWidth() + Buf_Margin, 0, LR_AnimationDuration);
				break;
			case OPEN:
				this.rightColumn_CurrentState = AniState.CLOSING;          
				this.AnimationScroller.startScroll(this.currentContentOffset, 0, -this.currentContentOffset, 0, LR_AnimationDuration);
				break;
			default:
				return false;
		}
		this.AnimationHandler.postDelayed(this.AnimationRunnable, AnimationPollingInterval);
		return true;
	}

	public boolean toggleleft(){		
		if(state != State.MAIN && state !=State.L_OUTLINE) return false;
		if(currentContentView != ContentState.C && currentContentView != ContentState.NEXTC && currentContentView != ContentState.COVER) return false;
		state = State.L_OUTLINE;
		switch(this.leftColumn_CurrentState) {
			case CLOSED:
				this.createBitmap(left_column, left_outline_image);
				this.left_outline_image.setVisibility(View.VISIBLE);
				this.leftColumn_CurrentState = AniState.OPENING;				
				switch(currentContentView){
					case C:
						this.content.setEnabled(false);
						break;
					case NEXTC:
						this.next_content.setEnabled(false);
						break;
				}
				this.AnimationScroller.startScroll(0, 0, this.getLeftWidth() - Buf_Margin, 0, LR_AnimationDuration);
				break;
			case OPEN:
				this.createBitmap(left_column, left_outline_image);
				this.left_outline_image.setVisibility(View.VISIBLE);
				this.left_column.setVisibility(View.INVISIBLE);
				this.leftColumn_CurrentState = AniState.CLOSING;          
				this.AnimationScroller.startScroll(this.currentContentOffset, 0, -this.currentContentOffset, 0, LR_AnimationDuration);
				break;
			default:
				return false;
		}
		Log.e("acim", "toggleleft");
		this.AnimationHandler.postDelayed(this.AnimationRunnable, AnimationPollingInterval);
		return true;
	}
	
	public boolean toggleNextContent(){
		if(state != State.MAIN) return false;
		if(rightColumn_CurrentState != AniState.CLOSED && leftColumn_CurrentState != AniState.CLOSED) return false;
		state = State.MAIN;		
		switch(this.currentContentView){
			case C:
				this.next_content.setVisibility(View.VISIBLE);
				next_content.setBackgroundResource(R.drawable.background);
				this.currentContentView = ContentState.TONEXT;
				this.content.setEnabled(false);
				this.next_content.setEnabled(false);
				this.AnimationScroller.startScroll(0, 0, 0, -this.getContentHeight(), content_AnimationDuration);
				break;
			case NEXTC:
				this.content.setVisibility(View.VISIBLE);
				content.setBackgroundResource(R.drawable.background);
				this.currentContentView = ContentState.TOC;
				this.content.setEnabled(false);
				this.next_content.setEnabled(false);
				this.AnimationScroller.startScroll(0, 0, 0, -this.getContentHeight(), content_AnimationDuration);
				break;
			default:
				return false;
		}
		this.AnimationHandler.postDelayed(this.AnimationRunnable, AnimationPollingInterval);
		return true;
	}
	
	public boolean toggleRightSetting(){
		if(state != State.MAIN && state !=State.R_SETTING) return false;
		if(currentContentView != ContentState.C && currentContentView != ContentState.NEXTC && currentContentView != ContentState.COVER) return false;
		state = State.R_SETTING;
		switch(this.rightSetting_CurrentState) {
			case CLOSED:
				this.right_setting.setVisibility(View.INVISIBLE);
				createBitmap(right_setting, right_setting_image);			
				this.right_setting_image.setVisibility(View.VISIBLE);				
				this.rightSetting_CurrentState = AniState.OPENING;
				switch(currentContentView){
					case C:
						this.content.setEnabled(false);
						break;
					case NEXTC:
						this.next_content.setEnabled(false);
						break;
				}
				this.AnimationScroller.startScroll(0, 0, -this.getSettingWidth() + Buf_Margin, 0, LR_AnimationDuration);
				break;
			case OPEN:
				this.right_setting.setVisibility(View.INVISIBLE);
				createBitmap(right_setting, right_setting_image);
				this.right_setting_image.setVisibility(View.VISIBLE);
				this.rightSetting_CurrentState = AniState.CLOSING;          
				this.AnimationScroller.startScroll(this.currentContentOffset, 0, -this.currentContentOffset, 0, LR_AnimationDuration);
				break;
			default:
				return false;
		}
		this.AnimationHandler.postDelayed(this.AnimationRunnable, AnimationPollingInterval);
		return true;
	}
	
	private int getRightWidth(){	
		return this.right_column.getLayoutParams().width;
	}

	private int getLeftWidth(){
		return this.left_column.getLayoutParams().width;
	}

	private int getContentHeight(){
		return this.content.getLayoutParams().height;
	}
	
	private int getSettingWidth(){
		return this.right_setting.getLayoutParams().width;
	}

	private void calculateChildDimensions(){
		Log.e("acim", "calculateChildDimensions");
		this.cover.getLayoutParams().height = this.getHeight();
		this.cover.getLayoutParams().width = this.getWidth();
		
		this.content.getLayoutParams().height = this.getHeight();
		this.content.getLayoutParams().width = this.getWidth();
		
		this.next_content.getLayoutParams().height = this.getHeight();
		this.next_content.getLayoutParams().width = this.getWidth();
		
		this.right_column.getLayoutParams().width = this.getWidth() - Margin + Buf_Margin;
		this.right_column.getLayoutParams().height = this.getHeight();

		this.left_column.getLayoutParams().width = this.getWidth() - Margin + Buf_Margin;
		this.left_column.getLayoutParams().height = this.getHeight();
		
		this.right_setting.getLayoutParams().width = this.getWidth() - Margin + Buf_Margin;
		this.right_setting.getLayoutParams().height = this.getHeight();
	}


	private void adjustContentPosition(boolean isAnimationOngoing){
		int scrollerOffset = 0;
		switch(state){
			case MAIN:
				//Log.e("acim", "adjustContentPosition MAIN");
				scrollerOffset = this.AnimationScroller.getCurrY();
				this.content.offsetTopAndBottom(scrollerOffset - this.currentContentOffset);
				this.next_content.offsetTopAndBottom(scrollerOffset - this.currentContentOffset);
				break;
			case L_OUTLINE:
			case R_DETAIL:
			case R_SETTING:
				scrollerOffset = this.AnimationScroller.getCurrX();
				switch(currentContentView){
					case COVER:
						//Log.e("acim", "adjustContentPosition LRR COVER");
						this.cover.offsetLeftAndRight(scrollerOffset - this.currentContentOffset);
						break;
					case C:
						//Log.e("acim", "adjustContentPosition LRR C");
						this.content.offsetLeftAndRight(scrollerOffset - this.currentContentOffset);
						break;
					case NEXTC:
						//Log.e("acim", "adjustContentPosition LRR NEXTC");
						this.next_content.offsetLeftAndRight(scrollerOffset - this.currentContentOffset);
						break;
					default:
						//Log.e("acim", "adjustContentPosition LRR ERROR");
						break;
				}
				break;
		}
		this.currentContentOffset = scrollerOffset;
		this.invalidate();

		if(isAnimationOngoing) {
			this.AnimationHandler.postDelayed(this.AnimationRunnable, AnimationPollingInterval);
		} else {			
			switch(state){
				case MAIN:					
					on_Content_TransitionComplete();
					break;
				case L_OUTLINE:
					on_LeftColumn_TransitionComplete();
					break;
				case R_DETAIL:
					on_RightColumn_TransitionComplete();
					break;
				case R_SETTING:
					on_RightSetting_TransitionComplete();
					break;
			}
		}
	}
	
	private void on_Content_TransitionComplete(){
		Log.e("acim","on_Content_TransitionComplete");
		switch(this.currentContentView){
			case TOC:
				this.currentContentView = ContentState.C;
				this.currentContentOffset = 0;
				this.content.setEnabled(true);
				this.next_content.setEnabled(false);
				this.next_content.setVisibility(View.GONE);
				next_content.setBackgroundResource(0);
				this.next_content.fullScroll(ScrollView.FOCUS_UP);
				break;
			case TONEXT:
				this.currentContentView = ContentState.NEXTC;
				this.currentContentOffset = 0;
				this.content.setEnabled(false);
				this.next_content.setEnabled(true);
				this.content.setVisibility(View.GONE);
				content.setBackgroundResource(0);
				this.content.fullScroll(ScrollView.FOCUS_UP);
				break;
			default:
				return;	
		}
	}

	private void on_RightColumn_TransitionComplete(){
		Log.e("acim","on_RightColumn_TransitionComplete");
		switch(this.rightColumn_CurrentState){
			case OPENING:
				this.rightColumn_CurrentState = AniState.OPEN;				
				break;
			case CLOSING:
				this.rightColumn_CurrentState = AniState.CLOSED;
				this.state = State.MAIN;
				switch(currentContentView){
					case C:
						this.content.setEnabled(true);
						break;
					case NEXTC:
						this.next_content.setEnabled(true);
						break;
				}				
				this.right_column.setVisibility(View.INVISIBLE);
				break;
			default:
				return;
		}
	}
	
	private void on_RightSetting_TransitionComplete(){
		Log.e("acim","on_RightSetting_TransitionComplete");
		switch(this.rightSetting_CurrentState){
			case OPENING:
				this.rightSetting_CurrentState = AniState.OPEN;
				this.right_setting.setVisibility(View.VISIBLE);
				this.right_setting_image.setVisibility(View.GONE);
				this.bm.recycle();
				break;
			case CLOSING:
				this.rightSetting_CurrentState = AniState.CLOSED;
				this.state = State.MAIN;
				switch(currentContentView){
					case C:
						this.content.setEnabled(true);
						break;
					case NEXTC:
						this.next_content.setEnabled(true);
						break;
				}
				this.right_setting_image.setVisibility(View.GONE);
				this.bm.recycle();
				break;
			default:
				return;
		}
	}

	private void on_LeftColumn_TransitionComplete(){
		Log.e("acim","on_LeftColumn_TransitionComplete");
		switch(this.leftColumn_CurrentState){
			case OPENING:
				this.left_column.setVisibility(View.VISIBLE);
				this.left_outline_image.setVisibility(View.GONE);
				this.bm.recycle();
				
				this.leftColumn_CurrentState = AniState.OPEN;
				break;
			case CLOSING:
				this.left_outline_image.setVisibility(View.GONE);
				this.bm.recycle();
				this.leftColumn_CurrentState = AniState.CLOSED;
				this.state = State.MAIN;
				switch(currentContentView){
					case C:
						this.content.setEnabled(true);
						break;
					case NEXTC:
						this.next_content.setEnabled(true);
						break;
				}
				this.left_column.setVisibility(View.INVISIBLE);
				break;
			default:
				return;
		}
	}

	protected class SmoothInterpolator implements Interpolator {

		@Override
		public float getInterpolation(float arg0) {
			// TODO Auto-generated method stub
			return (float)Math.pow(arg0-1, 5)+1;
		}		
	}

	protected class AnimationRunnable implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			boolean isAnimationOngoing = FlyOutContainer.this.AnimationScroller.computeScrollOffset();
			FlyOutContainer.this.adjustContentPosition(isAnimationOngoing);		
		}
	}
	
	private void createBitmap(View source, ImageView view){
		source.setDrawingCacheEnabled(true);
		source.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), 
	            MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		source.buildDrawingCache();
		bm = Bitmap.createBitmap(source.getDrawingCache());
		view.setImageBitmap(bm);
		source.destroyDrawingCache();
		source.setDrawingCacheEnabled(false);
	}
}
