package com.acim.acim_tw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Lesson implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1;
	
	public String filename;
	public int position;
	public String head;
	public String subject;
	//private CharSequence context;
	public List<Order_list> context;
	public String alarm[];
	public boolean bookmark;
	public HashMap<String, Comment> comment_hash;
	public ArrayList<Comment> comment_list;
	
	
	Lesson(){
		this.filename = "";
		this.position = -1;
		this.head = "";
		this.subject = "";
		//this.context = "empty";
		this.context = new ArrayList<Order_list>();
		this.alarm = new String[3];
		this.bookmark = false;
		this.comment_hash = new HashMap<String, Comment>();
		this.comment_list = new ArrayList<Comment>();
	}
	
	
	public void Load(Lesson l){
		this.head = l.getHead();
		this.subject = l.getSub();
		//this.context = l.getCon();
		this.alarm = l.getAla();
		this.bookmark = l.getBook();
		this.comment_hash = l.getComH();
		this.position = l.getPos();
		this.filename = l.getFile();
		this.comment_list = l.getComL();
	}
	//get
	public int getPos(){
		return position;
	}
	
	public String getHead(){
		return this.head;
	}
	
	public String getSub(){
		return this.subject;
	}
	
	public List<Order_list> getCon(){
		return this.context;
	}
	
	public String[] getAla(){
		return this.alarm;
	}
	
	public boolean getBook(){
		return this.bookmark;
	}
	
	public HashMap<String, Comment> getComH(){
		return this.comment_hash;
	}
	
	public ArrayList<Comment> getComL(){
		return this.comment_list;
	}
	
	public String getFile(){
		return filename;
	}
	//set
	public void setBook(boolean b){
		this.bookmark = b;
	}
	
	public void setPos(int p){
		this.position = p;
	}
}
