package com.acim.acim_tw;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.facebook.model.*;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CommentActivity extends Activity {

	ImageView back; 
	ImageView share;
	ImageView del;
	EditText comment;
	RelativeLayout record_button;
	
	private int mode = 0;
	private String comment_text = "";
	private String time = "";
	private int lesson = 0;
	private String unique_name = "";
	private int post = -1;
	
	private enum record_state { R_RECORDING, R_STOP, P_PLAYING, P_STOP };
	private record_state recordState;
	
	private ProgressBar posting;
	
	//錄製語音
	static String Record_path = "ACIMrecord";
	static String Record_file_type = ".amr";
	//static String Record_temp = "temp.amr";
	//TextView record;
	//TextView record_play;
	//TextView record_delete;
	boolean playing;
	//AlertDialog playing_dialog;
	//錄音視窗
	//AlertDialog record_dialog;
	//Button record_start;
	//boolean recording;
	MediaRecorder recorder;
	MediaPlayer player;
	FileOutputStream recordfile;
	static String record_finalfile_path;
	static File record_finalfile;
	//static String record_tempfile_path;
	//static File record_tempfile;
	static File record_outputDir;
	Handler update_record_button;
	boolean keyboard = false;
	
	String text_to_be_share;

	//最長錄音時間(分鐘)
	final int Max_time_to_record = 180;
	//--------------------------------------------------
	private int recorded_time = 0;	
	private ProgressBar progressbar_play, progressbar_rec;
	private ImageView icon;
	private boolean rec = false, touch=false;
	private TextView record_text;
	//---------------------------------------------------
	PopupWindow pw_delete;
	
	private static final String TAG = "MainActivity";  
    private UiLifecycleHelper uiHelper;  
    private LoginButton authButton ; 
    private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
    private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
    private boolean pendingPublishReauthorization = false;
    
    private Session.StatusCallback callback = new Session.StatusCallback() {  
        @Override  
        public void call(Session session, SessionState state, Exception exception) {  
            onSessionStateChange(session, state, exception);
        }  
    };
    
    private Request.Callback request_callback= new Request.Callback() {
        public void onCompleted(Response response) {
        	Log.e("acim", "request_callback");
        	posting.setVisibility(View.GONE);
        	if(response == null){
        		return;
        	} else{        		
        		Log.e("acim", response.toString());
        		if(response.getGraphObject() == null){
        			return;
        		} else {
        			Log.e("acim", response.getGraphObject().toString());
        			if(response.getGraphObject().getInnerJSONObject() == null){
        				return;
        			} else {
        				Log.e("acim", response.getGraphObject().getInnerJSONObject().toString());
        			}
        		}
        	}
            JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
            String postId = null;
            try {
                postId = graphResponse.getString("id");
            } catch (JSONException e) {
                Log.i(TAG,"JSON error "+ e.getMessage());
            }
            FacebookRequestError error = response.getError();
            if (error != null) {
          //  	Toast.makeText(getApplicationContext(),error.getErrorMessage(),
          //  			Toast.LENGTH_SHORT).show();
            } 
            else {
            	Toast.makeText(CommentActivity.this, getResources().getString(R.string.success_post), Toast.LENGTH_SHORT).show();
            }
        }
    };
    
    
    
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// TODO 自動產生的方法 Stub
		if(event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			Log.e("acim", event.toString());
			if(event.getAction() == KeyEvent.ACTION_UP){
				setResult();
				finish();
			}
			return true;
		}
		return super.dispatchKeyEvent(event);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_comment);
		
		Intent i = this.getIntent();
		Bundle b = i.getExtras();
		mode = b.getInt("mode");
		comment_text = b.getString("comment");
		time = b.getString("time");
		lesson = b.getInt("lesson");
		post = b.getInt("post");
		unique_name = time + lesson;
		
		String state = Environment.getExternalStorageState();
		if(Environment.MEDIA_MOUNTED.equals(state)){
			Log.e("acim", "ExternalStorage");
			record_outputDir = new File(Environment.getExternalStorageDirectory().getPath(), Record_path);
		} else {
			Log.e("acim", "InternalStorage");
			record_outputDir = new File(getFilesDir(), Record_path);
		}
		//確認存檔路徑
		//record_outputDir = new File(getFilesDir(), Record_path);
		if(!record_outputDir.exists()){
			record_outputDir.mkdir();
		}

		//record_tempfile = new File(record_outputDir, Record_temp);
		//record_tempfile_path = record_tempfile.getAbsolutePath();
		record_finalfile = new File(record_outputDir,  unique_name + Record_file_type);
		record_finalfile_path = record_finalfile.getAbsolutePath();

		Log.e("acim", "set record_finalfile " + record_finalfile_path);

		Initial();
		handleRecord();
		handleShare();
		
		posting = (ProgressBar)findViewById(R.id.progressBar_FBpost);
		posting.setVisibility(View.GONE);
		
		//---------------facebook

		uiHelper = new UiLifecycleHelper(this, callback);  
        uiHelper.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            pendingPublishReauthorization = savedInstanceState.getBoolean(PENDING_PUBLISH_KEY, false);
        }
        authButton = (LoginButton) findViewById(R.id.login_button);
		
		//-------------測試用字串---------------
		if(comment_text.equals("empty")) comment_text = "";
		//------------------------------------
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.comment, menu);
		return true;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		if(recorder != null) stop_record();
		if(player != null) stop_play();
		Log.e("acim", "CommentActivity onPause");
		update_record_button.removeCallbacks(update_button);
		update_record_button = null;
		comment_text = comment.getText().toString();
		recorder = null;
		player = null;
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Log.e("acim", "CommentActivity onResume");
		update_record_button = new Handler();		
		update_record_button.postDelayed(update_button, 500);
		comment.setText(comment_text);
		super.onResume();
		
		// For scenarios where the main activity is launched and user  
        // session is not null, the session state change notification  
        // may not be triggered. Trigger it if it's open/closed.  
        /*Session session = Session.getActiveSession();  
        if (session != null && (session.isOpened() || session.isClosed())) {  
            onSessionStateChange(session, session.getState(), null);  
        }*/
        uiHelper.onResume();
	}

	Runnable update_button = new Runnable() {
		
		@Override
		public void run() {
			// TODO 自動產生的方法 Stub
			switch(recordState){
				case P_PLAYING:
					progressbar_play.setVisibility(View.VISIBLE);
					progressbar_rec.setVisibility(View.GONE);
					if(player.isPlaying()) {
						progressbar_play.setProgress(player.getCurrentPosition()*100/player.getDuration());
					}
					break;
				case P_STOP:
					progressbar_play.setVisibility(View.VISIBLE);
					progressbar_rec.setVisibility(View.GONE);
					progressbar_play.setProgress(0);
					break;
				case R_RECORDING:
					progressbar_play.setVisibility(View.GONE);
					progressbar_rec.setVisibility(View.VISIBLE);
					recorded_time++;
					progressbar_rec.setProgress(recorded_time*100/Max_time_to_record);
					break;
				case R_STOP:
					progressbar_play.setVisibility(View.GONE);
					progressbar_rec.setVisibility(View.VISIBLE);
					recorded_time = 0;
					progressbar_rec.setProgress(0);
					break;
			}
			update_record_button.postDelayed(update_button, 500);
		}
	};

	private void Initial(){
		back = (ImageView)findViewById(R.id.comment_back);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult();
				finish();
			}
		});
		share = (ImageView)findViewById(R.id.comment_fb);
		comment = (EditText)findViewById(R.id.comment_text);
		del = (ImageView)findViewById(R.id.comment_del);
		del.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LayoutInflater lif = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
				View d_v = lif.inflate(R.layout.delete_window, null);
				pw_delete = new PopupWindow(d_v, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				TextView d_text = (TextView) d_v.findViewById(R.id.delete_text);
				d_text.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO 自動產生的方法 Stub
						comment.setText("");
						pw_delete.dismiss();
					}
				});				
				TextView d_record = (TextView) d_v.findViewById(R.id.delete_record);
				d_record.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO 自動產生的方法 Stub
						if(record_finalfile.exists()) {
							record_finalfile.delete();
							handleRecord();
						}
						pw_delete.dismiss();
					}
				});
				pw_delete.setContentView(d_v);
				pw_delete.setFocusable(true);
				pw_delete.setBackgroundDrawable(new BitmapDrawable());
				pw_delete.update();
				pw_delete.showAtLocation(v, Gravity.CENTER, 0, 0);
			}
		});
	}
	
	private void setResult(){
		Intent i = new Intent();
		Bundle b = new Bundle();
		b.putString("comment", comment.getText().toString());
		b.putString("time", time);
		b.putInt("post", post);
		
		if(record_finalfile.exists()) mode = 1; else mode = 0;
		b.putInt("mode", mode);
		
		i.putExtras(b);
		setResult(RESULT_OK, i);
	}
	
	private void handleRecord(){
		progressbar_rec = (ProgressBar)findViewById(R.id.progressbar_rec);
        progressbar_play = (ProgressBar)findViewById(R.id.progressbar_play);
        record_text = (TextView)findViewById(R.id.recored_button_text);
        
        if(!record_finalfile.exists()){
			recordState = record_state.R_STOP;
			progressbar_play.setVisibility(View.GONE);
			progressbar_rec.setVisibility(View.VISIBLE);
			record_text.setText(getResources().getString(R.string.add_new_record));
		} else {
			recordState = record_state.P_STOP;
			progressbar_play.setVisibility(View.VISIBLE);
			progressbar_rec.setVisibility(View.GONE);
			record_text.setText(getResources().getString(R.string.play));
		}
   
        icon = (ImageView)findViewById(R.id.icon);
        
        record_button = (RelativeLayout)findViewById(R.id.comment_record_button);
        record_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO 自動產生的方法 Stub
				switch(recordState){
					case P_PLAYING:
						recordState = record_state.P_STOP;
						stop_play();						
						record_text.setText(getResources().getString(R.string.play));
						break;
					case P_STOP:						
						start_play();						
						record_text.setText(getResources().getString(R.string.playing));
						break;
					case R_RECORDING:
						stop_record();
						recordState = record_state.P_STOP;
						record_text.setText(getResources().getString(R.string.play));
						break;
					case R_STOP:
						start_record();
						recordState = record_state.R_RECORDING;
						record_text.setText(getResources().getString(R.string.recording));
						break;
				}	
			}
		});        
        
        /*progressbar_rec.setOnClickListener(new OnClickListener(){
        	public void onClick(View v) {
        		touch = true;
        		tv.setText("語音錄音中 ...");
        		Thread();
			}
        });
        
        progressbar_play.setOnClickListener(new OnClickListener(){
        	public void onClick(View v) {
        		touch = true;
        		Thread();
			}
        });*/
        
	}
	
	private void stop_play(){
		player.stop();
        player.release();
        player = null;
	}
	
	private void start_play(){		 
        try {        	
    		player = new MediaPlayer();
        	player.reset();
            player.setLooping(false);
            player.setDataSource((new FileInputStream(record_finalfile)).getFD());
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                @Override
                public void onPrepared(MediaPlayer mp) {
                    // TODO 自動產生的方法 Stub
                    player.start();
                    recordState = record_state.P_PLAYING;
                }
            });                     
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    // TODO 自動產生的方法 Stub
                	recordState = record_state.P_STOP;
                	record_text.setText(getResources().getString(R.string.play));
                    player.stop();
                    player.release();
                    player = null;
                }
            });
            player.prepareAsync();
        } catch (IllegalArgumentException e) {
            // TODO 自動產生的 catch 區塊
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO 自動產生的 catch 區塊
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO 自動產生的 catch 區塊
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動產生的 catch 區塊
            e.printStackTrace();
        }
	}
	
	private void stop_record(){
		recorder.stop();
		recorder.release();
		recorder = null;
		//record_tempfile.renameTo(record_finalfile);
	}
	
	private void start_record(){
		recorder = new MediaRecorder();
        recorder.reset();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);                                                    
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        //先刪除舊的錄音檔，在將新的錄音檔重新命名                              
        if(record_finalfile.exists()){
        	record_finalfile.delete();
        }
        recorder.setOutputFile(record_finalfile_path);
        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            // TODO 自動產生的 catch 區塊
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動產生的 catch 區塊
            e.printStackTrace();
        }
	}
	 
    private void handleShare(){    	
    	share.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO 自動產生的方法 Stub
				text_to_be_share = comment.getText().toString();
				//--------將 text to be share 發布到 fb 的近況-------------
				publishStory(text_to_be_share);
				//-----------------------------------------------------
			}
		});
    }
    
	@Override
	protected void onDestroy() {
		// TODO 自動產生的方法 Stub
		Log.e("acim", "CommentActivity onDestroy");
		uiHelper.onDestroy();
		
		super.onDestroy();
		
	}

	@Override
	protected void onRestart() {
		// TODO 自動產生的方法 Stub
		Log.e("acim", "CommentActivity onRestart");
		super.onRestart();
	}

	@Override
	protected void onStart() {
		Log.e("acim", "CommentActivity onStart");
		// TODO 自動產生的方法 Stub
		super.onStart();
	}

	@Override
	protected void onStop() {
		Log.e("acim", "CommentActivity onStop");
		// TODO 自動產生的方法 Stub
		super.onStop();
	}
	
	@Override  
    public void onSaveInstanceState(Bundle outState) {  
        super.onSaveInstanceState(outState);  
        outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
        uiHelper.onSaveInstanceState(outState);  
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO 自動產生的方法 Stub
		Log.e("acim", "CommentActivity onActivityResult");
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}
	
	//--------------facebook

    

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		Log.e("acim", state.toString());
		if (state.isOpened()) { 
        	Log.e("acim", "onSessionStateChange isOpened.");
        	if (pendingPublishReauthorization && state.equals(SessionState.OPENED)){
        		Log.e("acim", "onSessionStateChange pendingPublishReauthorization && state.equals(SessionState.OPENED) is true and do repulic");
        		pendingPublishReauthorization = false;
        	    publishStory(text_to_be_share);
        	}
        	else if (pendingPublishReauthorization && state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
        		Log.e("acim", "onSessionStateChange pendingPublishReauthorization && state.equals(SessionState.OPENED_TOKEN_UPDATED) is true and do repulic");
        		pendingPublishReauthorization = false;
        	    publishStory(text_to_be_share);
        	}
        } else if (state.isClosed()){
        	Log.e("acim", "onSessionStateChange isClosed.");
        }
    }
    
    private void publishStory(String message) {
        Session session = Session.getActiveSession();
		
        if(!session.isOpened()) {
        	Log.e("acim", "publishStory !session.isOpened()");
        	pendingPublishReauthorization = true;
        	authButton.performClick();
        	Log.e("acim", "publishStory !session.isOpened() exit");
        }
        else if (session!=null){
        	Log.e("acim", "CommentActivity publishStory session.isOpened()");
            // Check for publish permissions    
            List<String> permissions = session.getPermissions();
            
            if (!isSubsetOf(PERMISSIONS, permissions)) {
                pendingPublishReauthorization = true;
                Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, PERMISSIONS);
                session.requestNewPublishPermissions(newPermissionsRequest);
                Log.e("acim", "publishStory requestNewPublishPermissions");
                return;
            }
            
            Bundle postParams = new Bundle();
            postParams.putString("message", message);            

            Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, request_callback);
        
            RequestAsyncTask task = new RequestAsyncTask(request);
            posting.setVisibility(View.VISIBLE);
            task.execute();            
            //comment.setText("");
            Log.e("acim", "publishStory succeed");
        }
    }
    
    private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }
        return true;
    }

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO 自動產生的方法 Stub
		if(posting.isShown()) return true;
		return super.dispatchTouchEvent(ev);
	}
    
    
}
