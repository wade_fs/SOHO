﻿package com.acim.acim_tw;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class CommentAdapter extends BaseAdapter {
	private Context mContext;
	private ImageView cur_del;
	private Button cur_delbtn;
	private float x,ux;
	private int width=60;
	private boolean trash=false;
//	private LinearLayout cur_container, cur_container2;
	private RelativeLayout cur_container, cur_container2;
		
	public CommentAdapter(Context mContext) {
		this.mContext = mContext;
	}

	public int getCount() {
		return MainActivity.lessonlist.current_lesson.comment_list.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup arg2) {
		ViewHolder viewHolder = null;
		if (view == null) {
			viewHolder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(R.layout.item, null);
			viewHolder.tvTitle = (TextView) view.findViewById(R.id.title);
			viewHolder.del_btn = (Button) view.findViewById(R.id.btn_del);
			viewHolder.del = (ImageView) view.findViewById(R.id.del);
			viewHolder.icon = (ImageView) view.findViewById(R.id.icon);
			viewHolder.container = (RelativeLayout) view.findViewById(R.id.container);
			viewHolder.container2 = (RelativeLayout) view.findViewById(R.id.container2);
			view.setTag(viewHolder);
		} 
		else {
			viewHolder = (ViewHolder) view.getTag();
		}
		
		//width = v.getHeight();

		view.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				final ViewHolder holder = (ViewHolder) v.getTag();
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					
					//設置背景为選中狀態
					//v.setBackgroundResource(R.drawable.mm_listitem_pressed);
					width = v.getHeight();
					x = event.getX();
					Log.e("acim", "down" + x);
					if ((cur_delbtn!=null)&&(cur_del!=null)&&(cur_del.getVisibility()==View.VISIBLE)&&(cur_delbtn.getVisibility()==View.VISIBLE)) {
						cur_del.setVisibility(View.GONE);
						cur_delbtn.setVisibility(View.GONE);
						if((cur_container2!=null)&&(cur_container!=null)){
							cur_container.scrollBy(-width, 0);
							cur_container2.scrollBy(-width, 0);
							Log.e("acim", "close");
						}
					}
				} 
				//		else if ((position!=0&&position!=1&&position!=(listsize-1)&&position!=(listsize-2))&&(event.getAction()== MotionEvent.ACTION_UP)) {
				else if ((event.getAction()== MotionEvent.ACTION_UP)) {
					// 松開處理
					//背景為未選中正常狀態
					//v.setBackgroundResource(R.drawable.mm_listitem_simple);
					width = v.getHeight();
					ux = event.getX();
					Log.e("acim", "up" + ux);
					if(trash==false){
						if ((holder.del!=null)&&(holder.del.getVisibility()==View.GONE)&&(holder.del_btn.getVisibility()==View.GONE)) {							
							if(Math.abs(x-ux) <=10){
								Comment c = MainActivity.lessonlist.current_lesson.comment_list.get(position);
								Intent i = new Intent();
								i.setClass(mContext, CommentActivity.class);
								Bundle b = new Bundle();
								b.putInt("post", position);
								b.putInt("mode", c.mode);
								b.putString("comment", c.comment);				
								b.putString("time", c.time);
								b.putInt("lesson", MainActivity.lessonlist.current_lesson.getPos());
								i.putExtras(b);
								((Activity) mContext).startActivityForResult(i, 321);
							} else {
								holder.del.setVisibility(View.VISIBLE);
								holder.del_btn.setVisibility(View.VISIBLE);
								trash = true;
								cur_del = holder.del;
								cur_delbtn = holder.del_btn;
								if((holder.container!=null)&&(holder.container2!=null)){
									holder.container.scrollBy(width, 0);
									holder.container2.scrollBy(width, 0);
									cur_container = holder.container;	
									cur_container2 = holder.container2;
									Log.e("acim", "open");
								}
							}
								//MainActivity.tv.setText(MainActivity.list.get(position).getText());
						}
					}
					else {
						trash = false;
					}
				} 
				else if (event.getAction() == MotionEvent.ACTION_MOVE) {
					//滑動時背景為選中狀態
					//v.setBackgroundResource(R.drawable.mm_listitem_pressed);
				} 
				else {
					//背景為未選中正常狀態
					//v.setBackgroundResource(R.drawable.mm_listitem_simple);
				}

				return true;
			}
		});

		viewHolder.tvTitle.setText(MainActivity.lessonlist.current_lesson.comment_list.get(position).comment);
		if(MainActivity.lessonlist.current_lesson.comment_list.get(position).mode == 0){
			viewHolder.icon.setImageResource(R.drawable.write_item);
		} else {
			viewHolder.icon.setImageResource(R.drawable.rec_item);
		}
		
		//點選刪除按鈕時刪除該項
		viewHolder.del_btn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				width = v.getHeight();
				if(cur_del!=null)
					cur_del.setVisibility(View.GONE);
				if(cur_delbtn!=null)
					cur_delbtn.setVisibility(View.GONE);
				if((cur_container2!=null)&&(cur_container!=null)){
					cur_container.scrollBy(-width, 0);
					cur_container2.scrollBy(-width, 0);
				}				
				MainActivity.lessonlist.current_lesson.comment_list.remove(position);
				notifyDataSetChanged();
			}
		});
		return view;
	}

	final static class ViewHolder {
		TextView tvTitle;
		ImageView del;
		ImageView icon;
		ImageView bot;
	//	LinearLayout container,container2;
		Button del_btn;
		
		RelativeLayout container,container2;
	}
}
