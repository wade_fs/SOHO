package com.acim.acim_tw;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.acim.acim_tw.FlyOutContainer.ContentState;

import view.MyScrollView;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.LeadingMarginSpan;
import android.text.style.StyleSpan;
import android.text.style.SuperscriptSpan;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


public class MainActivity extends Activity implements GestureDetector.OnGestureListener {

	//Load xml時需要
	static public String packetName;
	//字體大小設定
	//private int text_size_ratio = 10;
	
	public static int width;
	public static int height;
	boolean end;
	boolean start;
	boolean change_next_page;
	boolean next_page;
	//for right menu function
	LayoutInflater li;
	ImageView detail_back_button;
	//書簽
	static ImageView addbookmark_image;
	//
	ListView comment_list;
	//ArrayAdapter<String> comment_list_ada;
	CommentAdapter comment_list_ada2;
	//通知
	private TextView alarm_hint;
	private RelativeLayout alarm_default_alarm_switch;
	private ImageView alarm_default_alarm_image;
	private TextView alarm_default_alarm_text;
	
	private RelativeLayout alarm_cutsom_alarm_switch;
	private ImageView alarm_cutsom_alarm_image;
	private TextView alarm_cutsom_alarm_text;
	
	//for sliding menu
	FlyOutContainer root;

	//for gesture
	private GestureDetector gd;
	private static final int SWIPE_MIN_DISTANCE = 200;  
	private static final int SWIPE_THRESHOLD_VELOCITY = 25;
	//主頁面layout component
	private TextView title;
	private TextView subject;
	private TextView context;
	private static ImageView bookmark;
	private MyScrollView main_msv;
	//open status
	private boolean right_column_open_status = false;
	private boolean left_column_open_status = false;
	private boolean setting_column_open_status = false;
	//Lesson
	//static public List<Lesson> lesson_list;
	//static public Lesson current_lesson;
	static public Lesson_List lessonlist;
	//左欄
	private ListView outline;
	static public MyAdapter myada;
	private ImageView outline_back_button;
	//書頁
	private boolean cover = true;
	//設定
	private ImageView setting_back_button;
	private RelativeLayout setting_text_size;
	private TextView setting_text_size_hint;
	private TextView setting_text_preview_head;
	private TextView setting_text_preview_sub;
	private TextView setting_text_preview_con;
	private RelativeLayout setting_set_alarm;
	private ImageView setting_set_alarm_image;
	private TextView setting_set_alarm_text;
	private RelativeLayout setting_set_sound_or_vibrate;
	private ImageView setting_set_sov_image;
	private TextView setting_set_sov_text;
	//
	static public Resources res;
	//
	Comment c_to_be_save = null;
	int c_to_be_save_post = -1;
	//
	ArrayList<MotionEvent> temp_motion = new ArrayList<MotionEvent>();
	boolean motion_used;
	//
	Alarm alarm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("acim", "onCreate");
		
		//for diff size screen
		Display display = getWindowManager().getDefaultDisplay();
		Point p = new Point();
		display.getSize(p);
		width = p.x;
		height = p.y;
		
		this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_main, null);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//this.requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		//this.root.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
		this.setContentView(root);
		
		
		packetName = getApplicationInfo().packageName;
		
		res = this.getResources();
		lessonlist = new Lesson_List();
		//find all view required
		
		Initail();
		end = false;
		start = false;
		change_next_page = false;
		next_page = false;
		
		//gesture
		gd = new GestureDetector(this);
		root.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO 自動產生的方法 Stub
				Log.e("acim", "onTouch");
				return gd.onTouchEvent(event);
			}
		});
		//for right menu function
		li = LayoutInflater.from(MainActivity.this);
		
		//初始化gesture
		Initail_gensture();
		
		Initial_outline();
		//建立功能
		HandleAddBookMark();
		HandleComment();		
		HandleAlarm();				
		Handle_Setting();		
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO 自動產生的方法 Stub
		if(root.currentContentView == ContentState.TOC || root.currentContentView == ContentState.TONEXT){
			return true;
		}
		if(root.state == com.acim.acim_tw.FlyOutContainer.State.L_OUTLINE){
			if(ev.getX() > width - root.Margin)
			{
				toggle_left_column();
				return true;
			} else {
				return super.dispatchTouchEvent(ev);
			}
		} else if(root.state == com.acim.acim_tw.FlyOutContainer.State.R_SETTING){
			if(ev.getX() < root.Margin){
				toggle_right_setting();
				return true;
			} else {
				return super.dispatchTouchEvent(ev);
			}
		} else if(root.state == com.acim.acim_tw.FlyOutContainer.State.R_DETAIL){
			if(ev.getX() < root.Margin){
				toggle_right_column();
				return true;
			} else {
				return super.dispatchTouchEvent(ev);
			}
		}
		else if(!left_column_open_status && !right_column_open_status && gd.onTouchEvent(ev))
		{
			return true;
		}
		//Log.e("acim", "pass the dispatchTouchEvent");
		return super.dispatchTouchEvent(ev);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO 自動產生的方法 Stub
		Log.e("acim","onDown");
		if(main_msv.getEnd()){
			Log.e("acim","onDown: getEnd");
			end = true;
		}
		else {
			end = false;
		}
		if(main_msv.getStart()){
			Log.e("acim","onDown: getStart");
			start = true;
		}
		else {
			start = false;
		}
		return false;
	}
	

	@Override
	public boolean onFling(MotionEvent start_position, MotionEvent end_position, float velocityY,
			float velocityX) {
		// TODO Auto-generated method stub
		//Log.e("acim", "onFling" + start_position.getX() + " " + start_position.getY() + " " + end_position.getX() + " " + end_position.getY() + " " + velocityX + " " + velocityY);
		Log.e("acim","onFling");
		try{ 
			/*if(change_next_page){
				Log.e("acim","onFling: animation");
				change_next_page = false;
				return true;
			}
			else */
			if(start_position.getY() - end_position.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY)  
			{
				Log.e("acim","onfling up");
				if(cover){
					Log.e("acim","change lesson 0");
					Log.e("acim", "封面消失");
					root.disCover();
					cover = false;
					Log.e("acim", String.valueOf(lessonlist.current_lesson.position));
					if(lessonlist.current_lesson.position < 2) {
						Change_Lesson(2);
					}
					else {
						Change_Lesson(lessonlist.current_lesson.position);
					}
					return true;
				}				
			} 
			else if(end_position.getY() - start_position.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) 
			{
				Log.e("acim","onfling down");
			}
			else if(start_position.getX() - end_position.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) 
			{  
				Log.e("acim","onfling right");
				// right to left swipe
				if(cover){
					//setting_text_preview_head.setText("");
					//setting_text_preview_sub.setText("");
					//setting_text_preview_con.setText("");
					update_Setting();
					toggle_right_setting();					
				} else {
					Log.e("acim","toggle_right_column");
					toggle_right_column();
				}
				return true;
			}    
			else if(end_position.getX() - start_position.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) 
			{  
				Log.e("acim","onfling left");
				// left to right swipe
				Log.e("acim","toggle_left_column");
				toggle_left_column();
				return true;
			}  
		} 
		catch (Exception e) 
		{  
			// nothing  
		}
		return false;
	}

	@Override
	public void onLongPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		Log.e("acim", "onLongPress");		
	}

	@Override
	public boolean onScroll(MotionEvent start_position, MotionEvent end_position, float velocityY,
			float velocityX) {
		// TODO Auto-generated method stub
		if(cover){
			
		}
		else if(root.currentContentView == ContentState.TOC || root.currentContentView == ContentState.TONEXT){
			return true;
		} else if(start_position.getY() - end_position.getY() > SWIPE_MIN_DISTANCE){
			//Log.e("acim","onScroll: end is true");
			if(end){								
				if(lessonlist.lesson_list.size() > (lessonlist.current_lesson.getPos()+1)){
					Log.e("acim","onScroll: change lesson");
					change_next_page = true;
					next_page = true;
					Log.e("acim", "next lesson position " + String.valueOf(lessonlist.current_lesson.getPos()+1));
					Change_Lesson(lessonlist.current_lesson.getPos()+1);
					root.toggleNextContent();														
					end = false;
				}
				return true;
			} else {
				return false;
			}
		} else if(end_position.getY() - start_position.getY() > SWIPE_MIN_DISTANCE) {
			if(start){								
				cover = true;
				root.showCover();
				start = false;
				return true;
			} else {
				return false;
			}
		}
		/*else if (change_next_page){
			return true;
		}*/
		return false;
	}

	@Override
	public void onShowPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		Log.e("acim", "onShowPress");
	}

	@Override
	public boolean onSingleTapUp(MotionEvent arg0) {
		// TODO Auto-generated method stub
		Log.e("acim", "onSingleTapUp " + arg0.getX());
		/*if(arg0.getX() > 600){
			if(left_column_open_status) {
				toggle_left_column();
				return true;
			}
		} else if(arg0.getX() < 100){
			if(right_column_open_status){
				toggle_right_column();
				return true;
			}
		}*/
		return false;
	}
	
	public boolean toggle_right_column(){
		if(this.root.toggleright()){
			if(right_column_open_status==false){
				right_column_open_status = true;
			}
			else if(right_column_open_status==true){
				right_column_open_status = false;  
			}
			return true;
		} else {
			return false;
		}			
	}

	public boolean toggle_left_column(){
		if(this.root.toggleleft()) {
			if(left_column_open_status){
				left_column_open_status = false;
			}
			else{
				left_column_open_status = true;
			}
			return true;
		} else {
			return false;
		}		
	}
	
	public boolean toggle_right_setting(){		
		if(this.root.toggleRightSetting()){
			if(setting_column_open_status==false){
				setting_column_open_status = true;
			}
			else if(setting_column_open_status==true){
				setting_column_open_status = false;  
			}
			return true;
		} else {
			return false;
		}
	}
	
	private void Initail(){
		ProgressBar pb = (ProgressBar)findViewById(R.id.progress_loading);
		pb.setVisibility(View.INVISIBLE);
		pb.bringToFront();
		//middle
		TextPaint tp;		
		title = (TextView)findViewById(R.id.middle_text_head);
		tp = title.getPaint();
		tp.setFakeBoldText(true);		
		subject = (TextView)findViewById(R.id.middle_text_subject);
		tp = subject.getPaint();
		tp.setFakeBoldText(true);
		context = (TextView)findViewById(R.id.middle_text_context);
		bookmark = (ImageView)findViewById(R.id.middle_image_bookmark);
		main_msv = (MyScrollView)findViewById(R.id.middle_sv);
		//left outline
		outline = (ListView)findViewById(R.id.left_outline_list);
		outline_back_button = (ImageView) findViewById(R.id.left_outline_back);
		outline_back_button.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				toggle_left_column();
				return false;
			}
		});
		//right setting
		setting_back_button = (ImageView) findViewById(R.id.right_setting_back);
		setting_back_button.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				toggle_right_setting();
				return false;
			}
		});
		//right detail
		detail_back_button = (ImageView) findViewById(R.id.right_detail_back);
		detail_back_button.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				toggle_right_column();
				return false;
			}
		});		
		//alarm
		alarm = new Alarm(this, (AlarmManager) getSystemService(ALARM_SERVICE));
	}
	private void swap_page(){
		main_msv.setName("Old");
		if(next_page){
			if(root.currentContentView == ContentState.NEXTC){
				Log.e("acim","swap_page: page 1");
				TextPaint tp;	
				title = (TextView)findViewById(R.id.middle_text_head);
				tp = title.getPaint();
				tp.setFakeBoldText(true);		
				subject = (TextView)findViewById(R.id.middle_text_subject);
				tp = subject.getPaint();
				tp.setFakeBoldText(true);
				context = (TextView)findViewById(R.id.middle_text_context);
				bookmark = (ImageView)findViewById(R.id.middle_image_bookmark);
				main_msv.setStop(true);
				//main_msv.fullScroll(ScrollView.FOCUS_UP);
				main_msv = (MyScrollView)findViewById(R.id.middle_sv);
				main_msv.setStop(false);
			} else {
				Log.e("acim","swap_page: page 2");
				TextPaint tp;		
				title = (TextView)findViewById(R.id.next_middle_text_head);
				tp = title.getPaint();
				tp.setFakeBoldText(true);		
				subject = (TextView)findViewById(R.id.next_middle_text_subject);
				tp = subject.getPaint();
				tp.setFakeBoldText(true);
				context = (TextView)findViewById(R.id.next_middle_text_context);
				bookmark = (ImageView)findViewById(R.id.next_middle_image_bookmark);
				main_msv.setStop(true);
				//main_msv.fullScroll(ScrollView.FOCUS_UP);
				main_msv = (MyScrollView)findViewById(R.id.next_middle_sv);
				main_msv.setStop(false);
			}
			next_page = false;
		} else {
			if(root.currentContentView == ContentState.C){
				Log.e("acim","swap_page: page 1");
				TextPaint tp;	
				title = (TextView)findViewById(R.id.middle_text_head);
				tp = title.getPaint();
				tp.setFakeBoldText(true);		
				subject = (TextView)findViewById(R.id.middle_text_subject);
				tp = subject.getPaint();
				tp.setFakeBoldText(true);
				context = (TextView)findViewById(R.id.middle_text_context);
				bookmark = (ImageView)findViewById(R.id.middle_image_bookmark);
				main_msv.setStop(true);
				//main_msv.fullScroll(ScrollView.FOCUS_UP);
				main_msv = (MyScrollView)findViewById(R.id.middle_sv);
				main_msv.setStop(false);
			} else {
				Log.e("acim","swap_page: page 2");
				TextPaint tp;		
				title = (TextView)findViewById(R.id.next_middle_text_head);
				tp = title.getPaint();
				tp.setFakeBoldText(true);		
				subject = (TextView)findViewById(R.id.next_middle_text_subject);
				tp = subject.getPaint();
				tp.setFakeBoldText(true);
				context = (TextView)findViewById(R.id.next_middle_text_context);
				bookmark = (ImageView)findViewById(R.id.next_middle_image_bookmark);
				main_msv.setStop(true);
				//main_msv.fullScroll(ScrollView.FOCUS_UP);
				main_msv = (MyScrollView)findViewById(R.id.next_middle_sv);
				main_msv.setStop(false);
			}
		}
		main_msv.setName("Current");
	}	

	private void Initail_gensture(){
		 
		gd.setOnDoubleTapListener(new OnDoubleTapListener()  
		{  
			public boolean onDoubleTap(MotionEvent e)  
			{  
				return false;  
			}  

			public boolean onDoubleTapEvent(MotionEvent e)  
			{  
				/*
				//if the second tap hadn't been released and it's being moved  
				if(e.getAction() == MotionEvent.ACTION_MOVE)  
				{  
					
				}  
				else if(e.getAction() == MotionEvent.ACTION_UP&&!right_column_open_status)//user released the screen  
				{  
					Intent intent = new Intent();		
					intent.setClass(MainActivity.this, CopyrightActivity.class);	      
					startActivity(intent);
					MainActivity.this.finish(); 
				} 
				*/
				return false;  
			}  

			public boolean onSingleTapConfirmed(MotionEvent e)  
			{  
				return false;  
			}  
		});
	}

	private void Initial_outline(){
		myada = new MyAdapter(getLayoutInflater());
		outline.setAdapter(myada);
		outline.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO 自動產生的方法 Stub
				if(lessonlist.lesson_list.get(arg2).getPos() <=0){
					
				} else if(arg2 <= 1) {
					if(arg2 == 1){
						if(!cover){
							cover = true;
							root.showCover();
						} else {
							toggle_left_column();
						}
					}
				} else if(cover){					
					Log.e("acim", "封面消失");
					root.disCover();
					cover = false;		
					Change_Lesson(arg2);
				} else if(lessonlist.current_lesson.getPos() == arg2){
					//toggle_left_column();
				} else {
					Change_Lesson(arg2);
					Log.e("acim", "封面消失");
					root.disCover();
					cover = false;
					Change_Lesson(arg2);
				}
				toggle_left_column();
			}
		});
	}
	private void HandleAddBookMark(){
		//加入書簽部分
		//直接處理TableRow
		RelativeLayout addbookmark = (RelativeLayout) findViewById(R.id.right_detail_row1);
		addbookmark_image = (ImageView)findViewById(R.id.right_detail_imageAddBookMark);		
		addbookmark.setOnClickListener(new OnClickListener() {
			//在這裡加入按下加入書簽的功能
			@Override
			public void onClick(View arg0) {
				// TODO 自動產生的方法 Stub
				
				if(lessonlist.current_lesson.getBook()){
					
					lessonlist.current_lesson.setBook(false);
				} else {
					
					lessonlist.current_lesson.setBook(true);
				}
				//寫回
				//lessonlist.lesson_list.get(lessonlist.current_lesson.getPos()).Load(lessonlist.current_lesson);
				save_data();
				Set_bookmark_visible();
				myada.notifyDataSetChanged();
			}
		});
	}

	private void HandleAlarm(){
		alarm_hint = (TextView) findViewById(R.id.right_detail_alarm_hint);
		alarm_default_alarm_switch = (RelativeLayout)findViewById(R.id.right_detail_row4);
		alarm_default_alarm_image = (ImageView)findViewById(R.id.right_detail_image_switchDefaultAlarm);
		alarm_default_alarm_text = (TextView)findViewById(R.id.right_detail_text_swtichDefaultAlarm);
		alarm_default_alarm_switch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO 自動產生的方法 Stub
				if(lessonlist.alarm_switch == true && lessonlist.alarm_lesson == lessonlist.current_lesson.getPos() && lessonlist.custom_alarm == false){	
					lessonlist.alarm_switch = false;
					alarm.cancelAll();
				} else {
					lessonlist.alarm_switch = true;
					lessonlist.alarm_lesson = lessonlist.current_lesson.getPos();
					lessonlist.custom_alarm = false;
					
					int time_first_exe = Integer.valueOf(lessonlist.current_lesson.alarm[0]).intValue()*60;
					int times = Integer.valueOf(lessonlist.current_lesson.alarm[1]).intValue();
					int interval = Integer.valueOf(lessonlist.current_lesson.alarm[2]).intValue();	
					Log.e("acim", time_first_exe + ";" + times + ";" + interval);
					//---------------設定預定鬧鐘----------------
					alarm.setAlarm(time_first_exe, times, interval, lessonlist.vibrate);
					//----------------------------------------
				}
				
				update_Alarm_detail();
			}
		});
		
		alarm_cutsom_alarm_switch = (RelativeLayout)findViewById(R.id.right_detail_row5);
		alarm_cutsom_alarm_image = (ImageView)findViewById(R.id.right_detail_image_switchCustomAlarm);
		alarm_cutsom_alarm_text = (TextView)findViewById(R.id.right_detail_text_switchCustomAlarm);
		
		alarm_cutsom_alarm_switch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO 自動產生的方法 Stub
				if(lessonlist.alarm_switch == true && lessonlist.alarm_lesson == lessonlist.current_lesson.getPos() && lessonlist.custom_alarm == true){	
					lessonlist.alarm_switch = false;
					alarm.cancelAll();
					update_Alarm_detail();
				} else {
					float time_first_exe = Float.valueOf(lessonlist.current_lesson.alarm[0]).floatValue();
					int times = Integer.valueOf(lessonlist.current_lesson.alarm[1]).intValue();
					float interval = Float.valueOf(lessonlist.current_lesson.alarm[2]).floatValue();
					
					final View custom_alarm = li.inflate(R.layout.custom_alarm_input, null);
					
					EditText firsttime, inter, time;
					firsttime = (EditText)custom_alarm.findViewById(R.id.cai_edittext_first);
					inter = (EditText)custom_alarm.findViewById(R.id.cai_edittext_interval);
					time = (EditText)custom_alarm.findViewById(R.id.cai_edittext_times);
					firsttime.setFocusable(true);
					firsttime.requestFocus();
					//Log.e("acim_getAlarm", float_string(time_first_exe) + " , " + times + " , " + interval);
					firsttime.setText(float_string(time_first_exe));
					inter.setText(float_string(interval));
					time.setText(String.valueOf(times));
					
					AlertDialog.Builder bui= new AlertDialog.Builder(MainActivity.this);
					bui.setTitle("設定指定鬧鐘");
					bui.setView(custom_alarm);
					((EditText)custom_alarm.findViewById(R.id.cai_edittext_interval)).requestFocus();
					bui.setNegativeButton("取消", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO 自動產生的方法 Stub
							dialog.dismiss();
						}
					});
					bui.setPositiveButton("確定", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO 自動產生的方法 Stub	
							try{
								String f = ((EditText)custom_alarm.findViewById(R.id.cai_edittext_first)).getText().toString(),
										i = ((EditText)custom_alarm.findViewById(R.id.cai_edittext_interval)).getText().toString(),
										t = ((EditText)custom_alarm.findViewById(R.id.cai_edittext_times)).getText().toString();
								long time_first_exe = Long.valueOf(float_string(Float.valueOf(f).floatValue()*60*60*1000)).longValue();
								long interval = Long.valueOf(float_string(Float.valueOf(i).floatValue()*60*1000)).longValue();
								int times = Integer.valueOf(t).intValue();
								Log.e("acim", "設定自訂鬧鐘 " + interval + ";" + times);
								
								lessonlist.alarm_switch = true;
								lessonlist.alarm_lesson = lessonlist.current_lesson.getPos();
								lessonlist.custom_alarm = true;
	
								//---------------設定自訂鬧鐘----------------
								alarm.setAlarm(interval+time_first_exe, times, interval, lessonlist.vibrate);
								//alarm.setAlarm(interval, times, interval, lessonlist.vibrate);
								//----------------------------------------
								update_Alarm_detail();
							} catch(NumberFormatException e){
								e.printStackTrace();
								dialog.dismiss();
							}
						}
					});
					AlertDialog ad = bui.create();
					ad.show();
				}
			}
		});
	}
	
	private void update_Alarm_detail(){	
		SharedPreferences pref = getSharedPreferences("alarm", 0);
		lessonlist.alarm_switch = pref.getBoolean("switch", false);
		alarm_default_alarm_image.setImageResource(R.drawable.clock_disable);
		alarm_default_alarm_text.setText(getResources().getString(R.string.close_default_alarm));

		alarm_cutsom_alarm_image.setImageResource(R.drawable.clock_disable);
		alarm_cutsom_alarm_text.setText(getResources().getString(R.string.close_custom_alarm));
		
		if(lessonlist.alarm_switch == true && lessonlist.alarm_lesson == lessonlist.current_lesson.getPos()){
			if(lessonlist.custom_alarm == false){
				alarm_default_alarm_image.setImageResource(R.drawable.clock);
				alarm_default_alarm_text.setText(getResources().getString(R.string.open_default_alarm));		
			} else {
				alarm_cutsom_alarm_image.setImageResource(R.drawable.clock);
				alarm_cutsom_alarm_text.setText(getResources().getString(R.string.open_custom_alarm));
			}			
		}
	}
	
	private void Change_Lesson(int newlesson){
		swap_page();
		/*
		if(lessonlist.current_lesson.getPos() >= 0) {
			lessonlist.lesson_list.get(lessonlist.current_lesson.getPos()).Load(lessonlist.current_lesson);		
		}
		*/
		lessonlist.current_lesson.Load(lessonlist.lesson_list.get(newlesson));
		lessonlist.load_context();
		
		SpannableString ss = new SpannableString(lessonlist.current_lesson.getHead());
		ss.setSpan(new AbsoluteSizeSpan((int)(45*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);		
		title.setText(ss);
		ss = new SpannableString(lessonlist.current_lesson.getSub());
		ss.setSpan(new AbsoluteSizeSpan((int)(40*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);		
		subject.setText(ss);
		loatConext();	
		Set_bookmark_visible();
		//comment.setText(lessonlist.current_lesson.getCom());		
		
		if(lessonlist.current_lesson.getBook()){
			addbookmark_image.setImageResource(R.drawable.bookmark);			
		} else {
			addbookmark_image.setImageResource(R.drawable.bookmark_disable);		
		}
		
		update_comment_list();
		update_Alarm_detail();
		
		alarm_hint.setText(lessonlist.current_lesson.alarm[0]+getResources().getString(R.string.alarm_hint_part1)+lessonlist.current_lesson.alarm[1]+getResources().getString(R.string.alarm_hint_part2)+lessonlist.current_lesson.alarm[2]+getResources().getString(R.string.alarm_hint_part3));
		main_msv.fullScroll(ScrollView.FOCUS_UP);
	}
	
	private void loatConext(){
		CharSequence context_text = "";
		CharSequence text_temp = "";
		//boolean sup;
		List<Order_list> ol_list = lessonlist.current_lesson.context;
		for(int i = 0; i<ol_list.size(); i++){
			Order_list ol = ol_list.get(i);
			//context_text = "";
			for(int j = 0; j<ol.order_list.size(); j++){
				Order o = ol.order_list.get(j);
				/*if(o.sup.equals(String.valueOf(""))){
					sup = false;
				} else {
					sup = true;
				}*/
				SpannableString ss;
				if(!o.sup.equals("")){
					ss = new SpannableString("     "+o.sup); 
					ss.setSpan(new SuperscriptSpan(), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					ss.setSpan(new AbsoluteSizeSpan((int)(20*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					text_temp = TextUtils.concat(text_temp,ss);
				}
				
				Log.e("acim", o.text);				
				//if(o.text.length()-1 >= 0)Log.e("acim", Integer.valueOf(o.text.charAt(o.text.length()-1)).toString());
				if(o.text.charAt(o.text.length()-1) == 10) o.text = o.text.substring(0, o.text.length()-1);
				//o.text.replaceAll(String.valueOf('\10'), "");
				ss = new SpannableString(ToSBC(o.text));
				ss.setSpan(new AbsoluteSizeSpan((int)(35*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				text_temp = TextUtils.concat(text_temp,ss);
				
				if(o.practice) {
					SpannableString temp = new SpannableString(text_temp);
					temp.setSpan(new LeadingMarginSpan.Standard(50), 0, temp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					text_temp = TextUtils.concat("", temp);
				}
				
				context_text = TextUtils.concat(context_text,text_temp);
				text_temp = "";
			}
			context_text = TextUtils.concat(context_text,"\n");
		}
		context.setText(context_text);
	}
	
	static public void Set_bookmark_visible(){
		if(lessonlist.current_lesson.getBook()){
			addbookmark_image.setImageResource(R.drawable.bookmark);
			bookmark.setVisibility(View.VISIBLE);
		} else {			
			addbookmark_image.setImageResource(R.drawable.bookmark_disable);
			bookmark.setVisibility(View.GONE);
		}
	}
	
	private void Handle_Setting(){
		setting_text_size = (RelativeLayout)findViewById(R.id.rightSetting_row1);
		setting_text_size_hint = (TextView)findViewById(R.id.RS_textSize2);
		setting_text_preview_head = (TextView) findViewById(R.id.RS_textPreviewHead);		
		setting_text_preview_sub = (TextView) findViewById(R.id.RS_textPreviewSub);
		setting_text_preview_con = (TextView) findViewById(R.id.RS_textPreviewContext);		
		
		setting_text_size.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO 自動產生的方法 Stub
				switch(lessonlist.text_size_ratio){
					case 10:
						lessonlist.text_size_ratio = 14;
						set_preview_text();
						setting_text_size_hint.setText(getResources().getString(R.string.font_large));
						break;
					case 8:
						lessonlist.text_size_ratio = 10;
						set_preview_text();
						setting_text_size_hint.setText(getResources().getString(R.string.font_middle));
						break;
					case 14:
						lessonlist.text_size_ratio = 8;
						set_preview_text();
						setting_text_size_hint.setText(getResources().getString(R.string.font_small));
						break;
				}
			}
		});
		
		setting_set_alarm = (RelativeLayout)findViewById(R.id.rightSetting_row4);
		setting_set_alarm_image = (ImageView) findViewById(R.id.RS_imageAlarmSetting);
		setting_set_alarm_text = (TextView)findViewById(R.id.RS_textAlarmSetting);
		
		setting_set_alarm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO 自動產生的方法 Stub
				if(lessonlist.alarm_switch){
					setting_set_alarm_image.setImageResource(R.drawable.clock_disable);
					setting_set_alarm_text.setText(getResources().getString(R.string.alarm_set_is_close));
					lessonlist.alarm_switch = false;
					//---------------關閉鬧鐘-------------
					alarm.setSwitch(lessonlist.alarm_switch);
					//----------------------------------
				} else {
					setting_set_alarm_image.setImageResource(R.drawable.clock);
					setting_set_alarm_text.setText(getResources().getString(R.string.alarm_set_is_open));
					lessonlist.alarm_switch = true;
					//---------------開起鬧鐘-------------
					alarm.setSwitch(lessonlist.alarm_switch);
					//----------------------------------
				}
			}
		});
		
		setting_set_sound_or_vibrate = (RelativeLayout)findViewById(R.id.rightSetting_row5);		
		setting_set_sov_image = (ImageView) findViewById(R.id.RS_imageAlarmType);
		setting_set_sov_text = (TextView) findViewById(R.id.RS_textAlarmType);
		
		setting_set_sound_or_vibrate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO 自動產生的方法 Stub
				if(lessonlist.vibrate){
					setting_set_sov_image.setImageResource(R.drawable.alarm2);
					setting_set_sov_text.setText(getResources().getString(R.string.alarm_sound));
					lessonlist.vibrate = false;
					//--------------設定為聲音-----------------
					alarm.changeMode(lessonlist.vibrate);
					//--------------------------------------
				} else {
					setting_set_sov_image.setImageResource(R.drawable.alarm1);
					setting_set_sov_text.setText(getResources().getString(R.string.alarm_vibrate));
					lessonlist.vibrate = true;
					//---------------設定為震動----------------
					alarm.changeMode(lessonlist.vibrate);
					//--------------------------------------
				}
			}
		});		
	}
	
	private void update_Setting(){
		SharedPreferences pref = getSharedPreferences("alarm", 0);
		lessonlist.alarm_switch = pref.getBoolean("switch", false);
		switch(lessonlist.text_size_ratio){
			case 10:
				setting_text_size_hint.setText(getResources().getString(R.string.font_middle));
				break;
			case 8:
				setting_text_size_hint.setText(getResources().getString(R.string.font_small));
				break;
			case 14:
				setting_text_size_hint.setText(getResources().getString(R.string.font_large));
				break;
		}
		set_preview_text();
		
		if(lessonlist.alarm_switch){
			setting_set_alarm_image.setImageResource(R.drawable.clock);
			setting_set_alarm_text.setText(getResources().getString(R.string.alarm_set_is_open));
		} else {
			setting_set_alarm_image.setImageResource(R.drawable.clock_disable);
			setting_set_alarm_text.setText(getResources().getString(R.string.alarm_set_is_close));
		}
		
		if(lessonlist.vibrate){
			setting_set_sov_image.setImageResource(R.drawable.alarm1);
			setting_set_sov_text.setText(getResources().getString(R.string.alarm_vibrate));
		} else {
			setting_set_sov_image.setImageResource(R.drawable.alarm2);
			setting_set_sov_text.setText(getResources().getString(R.string.alarm_sound));
		}
	}
	
	private void set_preview_text(){
		CharSequence context_text = "";
		CharSequence text_temp = "";
		SpannableString ss = new SpannableString(getResources().getString(R.string.pre_part1));
		ss.setSpan(new AbsoluteSizeSpan((int)(45*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);		
		setting_text_preview_head.setText(ss);
		
		ss = new SpannableString(getResources().getString(R.string.pre_part2));
		ss.setSpan(new AbsoluteSizeSpan((int)(40*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);		
		setting_text_preview_sub.setText(ss);
		
		ss = new SpannableString(getResources().getString(R.string.pre_part3)); 		
		ss.setSpan(new AbsoluteSizeSpan((int)(35*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat(text_temp,ss);
		context_text = TextUtils.concat(context_text,text_temp,"\n");
		text_temp = "";
		
		ss = new SpannableString("2"); 
		ss.setSpan(new SuperscriptSpan(), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		ss.setSpan(new AbsoluteSizeSpan((int)(20*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat(text_temp,ss);		
		ss = new SpannableString(getResources().getString(R.string.pre_part4)); 		
		ss.setSpan(new AbsoluteSizeSpan((int)(35*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat(text_temp,ss);		
		SpannableString temp = new SpannableString(text_temp);
		temp.setSpan(new LeadingMarginSpan.Standard(50), 0, temp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat("", temp);
		context_text = TextUtils.concat(context_text,text_temp,"\n");
		text_temp = "";
		
		ss = new SpannableString("3"); 
		ss.setSpan(new SuperscriptSpan(), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		ss.setSpan(new AbsoluteSizeSpan((int)(20*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat(text_temp,ss);		
		ss = new SpannableString(getResources().getString(R.string.pre_part5)); 		
		ss.setSpan(new AbsoluteSizeSpan((int)(35*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat(text_temp,ss);		
		temp = new SpannableString(text_temp);
		temp.setSpan(new LeadingMarginSpan.Standard(50), 0, temp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat("", temp);
		context_text = TextUtils.concat(context_text,text_temp,"\n");
		text_temp = "";
		
		ss = new SpannableString("4"); 
		ss.setSpan(new SuperscriptSpan(), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		ss.setSpan(new AbsoluteSizeSpan((int)(20*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat(text_temp,ss);		
		ss = new SpannableString(getResources().getString(R.string.pre_part6)); 		
		ss.setSpan(new AbsoluteSizeSpan((int)(35*lessonlist.text_size_ratio/10)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat(text_temp,ss);		
		temp = new SpannableString(text_temp);
		temp.setSpan(new LeadingMarginSpan.Standard(50), 0, temp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		text_temp = TextUtils.concat("", temp);
		context_text = TextUtils.concat(context_text,text_temp,"\n");
		text_temp = "";		
		setting_text_preview_con.setText(context_text);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		Log.e("acim", "MainActivity onPause");
		try {					
			File f = new File(getFilesDir(), "temp_save"+res.getString(R.string.lesson_version));
			/*if(f.exists()){
				f.delete();			
			}*/
			FileOutputStream fs = new FileOutputStream(f);
			ObjectOutputStream os = new ObjectOutputStream(fs);
			ProgressBar pb = (ProgressBar)findViewById(R.id.progress_loading);
			pb.setVisibility(View.VISIBLE);
			pb.bringToFront();
			os.writeObject(lessonlist);
			os.close();			
			fs.close();
			pb.setVisibility(View.INVISIBLE);
			Log.e("acim", "save temp file");
			f.renameTo(new File(getFilesDir(), "save"+res.getString(R.string.lesson_version)));
			Log.e("acim", "rename file");
		} catch (FileNotFoundException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		}
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Log.e("acim", "MainActivity onResume");
		try {			
			File f = new File(getFilesDir(), "save"+res.getString(R.string.lesson_version));
			//f.delete();
			if(f.exists()){
				Log.e("acim", "file found");
				
				FileInputStream fs = new FileInputStream(f);
				ObjectInputStream os = new ObjectInputStream(fs);
				ProgressBar pb = (ProgressBar)findViewById(R.id.progress_loading);
				pb.setVisibility(View.VISIBLE);
				pb.bringToFront();
				lessonlist = (Lesson_List) os.readObject();
				os.close();
				fs.close();
				pb.setVisibility(View.INVISIBLE);
			} else {
				lessonlist.load_lesson();			
			} 
		} catch (FileNotFoundException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (StreamCorruptedException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();	
		} finally {
			if(c_to_be_save != null) {
				if(c_to_be_save_post >= 0){
					lessonlist.current_lesson.comment_hash.remove(c_to_be_save.time);
					lessonlist.current_lesson.comment_hash.put(c_to_be_save.time, c_to_be_save);
					lessonlist.current_lesson.comment_list.get(c_to_be_save_post).comment = c_to_be_save.comment;
					lessonlist.current_lesson.comment_list.get(c_to_be_save_post).mode = c_to_be_save.mode;
					lessonlist.current_lesson.comment_list.get(c_to_be_save_post).time = c_to_be_save.time;
				} else {
					lessonlist.current_lesson.comment_hash.put(c_to_be_save.time, c_to_be_save);
					lessonlist.current_lesson.comment_list.add(c_to_be_save);
				}
				save_data();
				c_to_be_save = null;
				update_comment_list();
			}
			//-------------更新alarm開關的狀態-----------------
			//----------------------------------------------
			update_Setting();			
		}
		super.onResume();
	}
	
	private void update_comment_list(){ 
		//comment_list_ada.clear();
		/*for(int i = 0; i<lessonlist.current_lesson.comment_list.size(); i++){
			Log.e("acim", "ChangeLesson add comment " + i);
			Comment c = lessonlist.current_lesson.comment_list.get(i);
			comment_list_ada.add(c.mode + ";" + c.time + ";" + c.comment);
		}
		comment_list_ada.notifyDataSetChanged();*/
		comment_list_ada2.notifyDataSetChanged();
	}
	
	private void HandleComment(){
		RelativeLayout rl_comment = (RelativeLayout)findViewById(R.id.right_detail_row2);
		rl_comment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(MainActivity.this, CommentActivity.class);
				Bundle b = new Bundle();
				b.putInt("post", -1);
				b.putInt("mode", 0);
				b.putString("comment", "empty");
				Date d = new Date();
				String s = String.valueOf(d.getYear())+
						String.valueOf(d.getMonth())+
						String.valueOf(d.getDate())+
						String.valueOf(d.getHours())+
						String.valueOf(d.getMinutes())+
						String.valueOf(d.getSeconds());
				b.putString("time", s);
				b.putInt("lesson", lessonlist.current_lesson.getPos());
				i.putExtras(b);
				startActivityForResult(i, 123);
			}
		});
		
		comment_list = (ListView)findViewById(R.id.right_detail_comment_list);
		//comment_list_ada = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		comment_list_ada2 = new CommentAdapter(this);
		//comment_list.setAdapter(comment_list_ada);
		comment_list.setAdapter(comment_list_ada2);
/*		
		comment_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO 自動產生的方法 Stub
				Comment c = lessonlist.current_lesson.comment_list.get(arg2);
				Intent i = new Intent();
				i.setClass(MainActivity.this, CommentActivity.class);
				Bundle b = new Bundle();
				b.putInt("post", arg2);
				b.putInt("mode", c.mode);
				b.putString("comment", c.comment);				
				b.putString("time", c.time);
				b.putInt("lesson", lessonlist.current_lesson.getPos());
				i.putExtras(b);
				startActivityForResult(i, 321);
			}
		});
*/		
	}
	
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO 自動產生的方法 Stub
		Log.e("acim", "onActivityResult " + requestCode + ";" + resultCode);
		if(requestCode == 123 && resultCode == RESULT_OK){
			Log.e("acim", "onActivityResult get data");
			Bundle b = data.getExtras();
			String com = b.getString("comment");
			String tim = b.getString("time");
			int mod = b.getInt("mode");
			c_to_be_save = new Comment();
			c_to_be_save.comment = com;
			c_to_be_save.mode = mod;
			c_to_be_save.time = tim;
			c_to_be_save_post = -1;
		} else if(requestCode == 321 && resultCode == RESULT_OK){
			Log.e("acim", "onActivityResult get data edited");
			Bundle b = data.getExtras();
			String com = b.getString("comment");
			String tim = b.getString("time");
			int mod = b.getInt("mode");
			c_to_be_save = new Comment();
			c_to_be_save.comment = com;
			c_to_be_save.mode = mod;
			c_to_be_save.time = tim;
			c_to_be_save_post = b.getInt("post");
		}
		if(c_to_be_save.comment.trim().equals("") && c_to_be_save.mode == 0){
			c_to_be_save = null;
		} else if(c_to_be_save.comment.trim().equals("") && c_to_be_save.mode == 1){
		    c_to_be_save.comment = getResources().getString(R.string.text_onlyrecord);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void save_data(){
		lessonlist.lesson_list.get(lessonlist.current_lesson.getPos()).Load(lessonlist.current_lesson);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Log.e("acim", "MainActivity onDestory");
		root.removeAllViews();
		super.onDestroy();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		Log.e("acim", "MainActivity onRestart");
		super.onRestart();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		Log.e("acim", "MainActivity onStart");
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Log.e("acim", "MainActivity onStop");
		super.onStop();
	}
	
	public String ToSBC(String input) {
		// 半角转全角：
		char[] c = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == 32) {
				c[i] = (char) 12288;
				continue;
			}
			if (c[i] < 127 && c[i]>32)
				c[i] = (char) (c[i] + 65248);
		}
		return new String(c);
	}

	private String float_string(float f){
		String result = "";
		result = (f == (int)f? String.format("%d", (int)f) : Float.valueOf(f).toString()); 
		return result;
	}
		
}
